﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.AsTrans.RNDC.Infrastructure.Data.Main.EF
{

  public partial class SPX_RNDC_PRODEntities
  {
    private const string DefaultIsolationCmd = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED";
    //private const string DefaultIsolationCmd = "SET TRANSACTION ISOLATION LEVEL SNAPSHOT";

    partial void OnContextCreated()
    {
      this.ExecuteStoreCommand(DefaultIsolationCmd);
    }
  }

}
