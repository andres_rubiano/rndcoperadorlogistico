﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Simplexity.AsTrans.RNDC.BusinessRules.UI.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.UI.Services;
using Simplexity.AsTrans.RNDC.UI.Management.Properties;
using Simplexity.AsTrans.RNDC.UI.Management.SPX_RNDC_PRODDataSetDetailTableAdapters;
using Simplexity.AsTrans.RNDC.UI.Management.SPX_RNDC_PRODDataSetRequestTableAdapters;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Services;
using Simplexity.AsTrans.RNDC.Infrastructure.Data.Main.EF;
using Timer = System.Timers.Timer;


namespace Simplexity.AsTrans.RNDC.UI.Management
{
  public partial class Management : Form
  {
    private string _requestId;
    private int _processId;
    delegate void SetTextCallback(string text);
    private Thread _thread = null;
   
    public Management()
    {
      InitializeComponent();
      try
      {
        EncryptConfigSection("connectionStrings");
      }
      catch (Exception ex)
      {
        ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
        MessageBox.Show(Resources.LoadError + ex);
      }
      
    }
    
    private void Form1_Load(object sender, EventArgs e)
    {
      try
      {
        if (new RequestServices().GetVersion())
        {
          sp_Rndc_GetRequestTableAdapter.Connection.ConnectionString = requestDetailTableAdapter.Connection.ConnectionString = fieldsToQueryTableAdapter.Connection.ConnectionString = GetConnectionStringByName("RNDCDB");
          // TODO: This line of code loads data into the 'sPX_RNDC_PRODDataSetFieldsToQuery.FieldsToQuery' table. You can move, or remove it, as needed.
          fieldsToQueryTableAdapter.Fill(sPX_RNDC_PRODDataSetFieldsToQuery.FieldsToQuery);
          // TODO: This line of code loads data into the 'sPX_RNDC_PRODDataSetDetail.RequestDetail' table. You can move, or remove it, as needed.
          requestDetailTableAdapter.Fill(sPX_RNDC_PRODDataSetDetail.RequestDetail);
          // TODO: This line of code loads data into the 'sPX_RNDC_PRODDataSetRequest.Sp_Rndc_GetRequest' table. You can move, or remove it, as needed.
          sp_Rndc_GetRequestTableAdapter.Fill(sPX_RNDC_PRODDataSetRequest.Sp_Rndc_GetRequest);
          LblDetalle.Visible = false;
          DGVDetalle.Visible = false;
          LblMostrarDetalles.Visible = false;
          CboxMostrarDetalles.Visible = false;
          CboxMostrar.SelectedIndex = 0;
          TBoxFiltro.Text = string.Empty;
          LblVersion.Text = LblVersion2.Text += System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

          LblRPendientes.Text = GetStatus(0);
          LblRProcesadas.Text = GetStatus(1);
          LblRTotal.Text = GetStatus(2);
          int interval;
          int.TryParse(ConfigurationManager.AppSettings["INTERVAL"], out interval);
          var timer = new Timer();
          timer.Elapsed += LoadStatus;
          timer.Interval = interval;
          timer.Enabled = true;
          timer.AutoReset = true;
        }
        else
        {
          MessageBox.Show(Resources.VersionError, Resources.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
          Close();
        }
      }
      catch (Exception ex)
      {
        ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
        MessageBox.Show(Resources.LoadError + ex);
      }
    }

    private static void EncryptConfigSection(string sectionKey)
    {
	  //var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
	  //var section = config.GetSection(sectionKey);
	  //if (section != null)
	  //{
	  //  if (!section.SectionInformation.IsProtected)
	  //  {
	  //	if (!section.ElementInformation.IsLocked)
	  //	{
	  //	  section.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
	  //	  section.SectionInformation.ForceSave = true;
	  //	  config.Save(ConfigurationSaveMode.Full);
	  //	}
	  //  }
	  //}
    }
    
    private void dataGridView1_RowHeaderMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
    {
      _requestId = dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString();
      DGVDetalle.Visible = true;
      LblDetalle.Visible = true;
      LblMostrarDetalles.Visible = true;
      CboxMostrarDetalles.Visible = true;
      CboxMostrarDetalles.SelectedIndex = 0;
      textBox1.Text = string.Empty;
      textBox2.Text = string.Empty;
      requestDetailTableAdapter.GetDetails(sPX_RNDC_PRODDataSetDetail.RequestDetail,10,int.Parse(_requestId));
    }

    private void LoadStatus(object sender, ElapsedEventArgs elapsedEventArgs)
    {
      _thread =
        new Thread(ThreadProcSafe);
      _thread.Start();
    }

    private void ThreadProcSafe()
    {
      SetTextLblPendientes(GetStatus(0));
      SetTextLblProcesadas(GetStatus(1));
      SetTextLblTotal(GetStatus(2));
    }

    private void SetTextLblPendientes(string text)
    {
      // InvokeRequired required compares the thread ID of the
      // calling thread to the thread ID of the creating thread.
      // If these threads are different, it returns true.
      if (LblRPendientes.InvokeRequired)
      {
        SetTextCallback d = SetTextLblPendientes;
        Invoke(d, new object[] { text });
      }
      else
      {
        LblRPendientes.Text = text;
      }
    }
    private void SetTextLblProcesadas(string text)
    {
      // InvokeRequired required compares the thread ID of the
      // calling thread to the thread ID of the creating thread.
      // If these threads are different, it returns true.
      if (LblRProcesadas.InvokeRequired)
      {
        SetTextCallback d = SetTextLblProcesadas;
        Invoke(d, new object[] { text });
      }
      else
      {
        LblRProcesadas.Text = text;
      }
    }

    private void SetTextLblTotal(string text)
    {
      // InvokeRequired required compares the thread ID of the
      // calling thread to the thread ID of the creating thread.
      // If these threads are different, it returns true.
      if (LblRTotal.InvokeRequired)
      {
        SetTextCallback d = SetTextLblTotal;
        Invoke(d, new object[] { text });
      }
      else
      {
        LblRTotal.Text = text;
      }
    }

    private static string GetStatus(int status)
    {
      if(status == 1||status== 0)
      {
        using (var requestEntities = new SPX_RNDC_PRODEntities())
        {
          int count = (from p in requestEntities.Request
                       where p.Status == status
                       select p
                       ).Count();
          return count.ToString();
        }
      }
      using (var requestEntities = new SPX_RNDC_PRODEntities())
      {
        int count = (from p in requestEntities.Request
                     select p
                    ).Count();
        return count.ToString();
      }
    }

    private void BtnAgregarSolicitud_Click(object sender, EventArgs e)
    {
      try
      {
        if (CBoxInterface.SelectedIndex == -1 || String.IsNullOrEmpty(TBoxReferencia.Text))
        {
          MessageBox.Show(Resources.Reference, Resources.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        else
        {
          DialogResult boton = MessageBox.Show(Resources.AddRequest, Resources.Alert, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
          if (boton == DialogResult.OK)
          {
            var InterfaceCode = CBoxInterface.SelectedItem.ToString();
            var InterfaceReference = TBoxReferencia.Text;

            var dataBase = DatabaseFactory.CreateDatabase("RNDCDB");
            var cmd = dataBase.GetStoredProcCommand("Sp_Rndc_InsertRequest");
            dataBase.AddInParameter(cmd, "@InterfaceCode", DbType.String, InterfaceCode);
            dataBase.AddInParameter(cmd, "@InterfaceReference", DbType.String, InterfaceReference);

            dataBase.ExecuteDataSet(cmd);
            BtnActualizar(sender, e);
          }
        }
      }
      catch (Exception ex)
      {

        ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
      }
      
    }

    private void dataGridView2_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      string xmlRequest, xmlResponse;
      xmlRequest = DGVDetalle[7, DGVDetalle.CurrentCell.RowIndex].Value.ToString();
      xmlResponse = DGVDetalle[8, DGVDetalle.CurrentCell.RowIndex].Value.ToString();
      if(!String.IsNullOrEmpty(xmlRequest)||!String.IsNullOrEmpty(xmlResponse))
      {
        var doc = XDocument.Parse(xmlRequest);
        textBox1.Text = doc.ToString(); //xmlRequest;
        doc = XDocument.Parse(xmlResponse);
        textBox2.Text = doc.ToString();
      }
      else
      {
        textBox1.Text = string.Empty;
        textBox2.Text = string.Empty;
      }
    }

    private void CBoxTipoconsulta_SelectedIndexChanged(object sender, EventArgs e)
    {
      DGVCampos.Visible = true;
      ChBoxTodos.Visible = true;
      ChBoxTodos.Checked = false;
      int process = CBoxTipoconsulta.SelectedIndex;
      if(process==0)
      {
        _processId = (int)RequestType.ProcessId.CrearOActualizarDatosDeVehiculo;
      }else
      {
        _processId = (int)RequestType.ProcessId.CrearOActualizarDatosDeTercero;
      }

      fieldsToQueryTableAdapter.GetFieldsToQuery(sPX_RNDC_PRODDataSetFieldsToQuery.FieldsToQuery, _processId);
    }

    private void BtnConsultar_Click(object sender, EventArgs e)
    {
      try
      {
        bool _hasQuery = false;
        if (CBoxTipoconsulta.SelectedIndex == -1)
        {
          MessageBox.Show(Resources.TypeQuerySelect, Resources.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        else
        {
          var list = new List<ClassQuery>();
          foreach (DataGridViewRow fields in DGVCampos.Rows)
          {
            if (fields.Cells[2].Value == null && fields.Cells[2].RowIndex == 0)
            {
              MessageBox.Show(Resources.NitValid, Resources.Alert, MessageBoxButtons.OK,
                              MessageBoxIcon.Warning);
              break;
            }
            if (fields.Cells[0].Value != null && fields.Cells[1].Value != null && fields.Cells[2].Value != null)
            {
              list.Add(new ClassQuery
              {
                field = fields.Cells[0].Value.ToString(),
                fielQuery = bool.Parse(fields.Cells[1].Value.ToString()),
                fieldCondition = fields.Cells[2].Value.ToString()
              });
              _hasQuery = true;
            }
            else if (fields.Cells[0].Value != null && fields.Cells[1].Value != null)
            {
              list.Add(new ClassQuery
              {
                field = fields.Cells[0].Value.ToString(),
                fielQuery = bool.Parse(fields.Cells[1].Value.ToString())
              });
              _hasQuery = true;
            }
            else if (fields.Cells[0].Value != null && fields.Cells[2].Value != null)
            {
              list.Add(new ClassQuery
              {
                field = fields.Cells[0].Value.ToString(),
                fieldCondition = fields.Cells[2].Value.ToString()
              });
              _hasQuery = true;
            }
          }
          if (_hasQuery)
          {
            var response = new QueryRequest().GetImformation(list, _processId);
            FormatData(response);
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
      }
      
    }

    private void FormatData(string xml )
    {
      var document = XDocument.Parse(xml);
      var ds = new DataSet();
      ds.ReadXml(document.CreateReader());
      DGVResultados.AutoGenerateColumns = true;
      DGVResultados.DataSource = ds.Tables[0];
    }

    private void ChBoxTodos_CheckedChanged(object sender, EventArgs e)
    {
      if(ChBoxTodos.Checked)
      {
        foreach (DataGridViewRow row in DGVCampos.Rows)
        {
          row.Cells[1].Value = true;
        }
      }else
      {
        foreach (DataGridViewRow row in DGVCampos.Rows)
        {
          row.Cells[1].Value = false;
        }
      }
    }

    private void CboxMostrar_SelectedIndexChanged(object sender, EventArgs e)
    {
	  //new Sp_Rndc_GetRequestTableAdapter().GetRequest(sPX_RNDC_PRODDataSetRequest.Sp_Rndc_GetRequest,int.Parse(CboxMostrar.Text));
    }

    private void CboxMostrarDetalles_SelectedIndexChanged(object sender, EventArgs e)
    {
      new RequestDetailTableAdapter().GetDetails(sPX_RNDC_PRODDataSetDetail.RequestDetail,
                                                 int.Parse(CboxMostrarDetalles.Text), int.Parse(_requestId));
    }

    private void BtnFiltro_Click(object sender, EventArgs e)
    {
      new Sp_Rndc_GetRequestTableAdapter().GetByReference(sPX_RNDC_PRODDataSetRequest.Sp_Rndc_GetRequest,
                                                          TBoxFiltro.Text);
    }

    private void BtnActualizar(object sender, EventArgs e)
    {
      // TODO: This line of code loads data into the 'sPX_RNDC_PRODDataSetRequest.Sp_Rndc_GetRequest' table. You can move, or remove it, as needed.
      sp_Rndc_GetRequestTableAdapter.Fill(sPX_RNDC_PRODDataSetRequest.Sp_Rndc_GetRequest);
      LblDetalle.Visible = false;
      DGVDetalle.Visible = false;
      LblMostrarDetalles.Visible = false;
      CboxMostrarDetalles.Visible = false;
      CboxMostrar.SelectedIndex = 0;
      TBoxFiltro.Text = TBoxReferencia.Text = textBox1.Text = textBox2.Text = string.Empty;

      
    }

    // Retrieves a connection string by name.
    // Returns null if the name is not found.
    static string GetConnectionStringByName(string name)
    {
      // Assume failure.
      string returnValue = null;

      // Look for the name in the connectionStrings section.
      var settings =
      ConfigurationManager.ConnectionStrings[name];

      // If found, return the connection string.
      if (settings != null)
        returnValue = settings.ConnectionString;

      return returnValue;
    }
  }
}
