﻿namespace Simplexity.AsTrans.RNDC.UI.Management
{
  partial class Management
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Management));
			this.TCMonitoreo = new System.Windows.Forms.TabControl();
			this.TBMonitoreo = new System.Windows.Forms.TabPage();
			this.BtnFiltro = new System.Windows.Forms.Button();
			this.TBoxFiltro = new System.Windows.Forms.TextBox();
			this.LblFiltro = new System.Windows.Forms.Label();
			this.LblMostrarDetalles = new System.Windows.Forms.Label();
			this.CboxMostrarDetalles = new System.Windows.Forms.ComboBox();
			this.LblMostrar = new System.Windows.Forms.Label();
			this.CboxMostrar = new System.Windows.Forms.ComboBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.spRndcGetRequestBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.sPX_RNDC_PRODDataSetRequest = new Simplexity.AsTrans.RNDC.UI.Management.SPX_RNDC_PRODDataSetRequest();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.DGVDetalle = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.requestDetailBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.sPX_RNDC_PRODDataSetDetail = new Simplexity.AsTrans.RNDC.UI.Management.SPX_RNDC_PRODDataSetDetail();
			this.LblDetalle = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button3 = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.BtnAgregarSolicitud = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.LblReferencia = new System.Windows.Forms.Label();
			this.CBoxInterface = new System.Windows.Forms.ComboBox();
			this.LblInterface = new System.Windows.Forms.Label();
			this.TBoxReferencia = new System.Windows.Forms.TextBox();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.LblRTotal = new System.Windows.Forms.Label();
			this.LblRProcesadas = new System.Windows.Forms.Label();
			this.LblRPendientes = new System.Windows.Forms.Label();
			this.LblTotal = new System.Windows.Forms.Label();
			this.LblProcesadas = new System.Windows.Forms.Label();
			this.LblPendientes = new System.Windows.Forms.Label();
			this.LblVersion = new System.Windows.Forms.Label();
			this.TbConsulta = new System.Windows.Forms.TabPage();
			this.ChBoxTodos = new System.Windows.Forms.CheckBox();
			this.DGVResultados = new System.Windows.Forms.DataGridView();
			this.BtnConsultar = new System.Windows.Forms.Button();
			this.LblResultados = new System.Windows.Forms.Label();
			this.DGVCampos = new System.Windows.Forms.DataGridView();
			this.fieldDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Consultar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.Criterio = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fieldsToQueryBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.sPX_RNDC_PRODDataSetFieldsToQuery = new Simplexity.AsTrans.RNDC.UI.Management.SPX_RNDC_PRODDataSetFieldsToQuery();
			this.CBoxTipoconsulta = new System.Windows.Forms.ComboBox();
			this.lblTipConsulta = new System.Windows.Forms.Label();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.LblVersion2 = new System.Windows.Forms.Label();
			this.fieldsToQueryTableAdapter = new Simplexity.AsTrans.RNDC.UI.Management.SPX_RNDC_PRODDataSetFieldsToQueryTableAdapters.FieldsToQueryTableAdapter();
			this.requestDetailTableAdapter = new Simplexity.AsTrans.RNDC.UI.Management.SPX_RNDC_PRODDataSetDetailTableAdapters.RequestDetailTableAdapter();
			this.sp_Rndc_GetRequestTableAdapter = new Simplexity.AsTrans.RNDC.UI.Management.SPX_RNDC_PRODDataSetRequestTableAdapters.Sp_Rndc_GetRequestTableAdapter();
			this.TCMonitoreo.SuspendLayout();
			this.TBMonitoreo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spRndcGetRequestBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.sPX_RNDC_PRODDataSetRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DGVDetalle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.requestDetailBindingSource1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.sPX_RNDC_PRODDataSetDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.TbConsulta.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.DGVResultados)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DGVCampos)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fieldsToQueryBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.sPX_RNDC_PRODDataSetFieldsToQuery)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			this.SuspendLayout();
			// 
			// TCMonitoreo
			// 
			this.TCMonitoreo.Controls.Add(this.TBMonitoreo);
			this.TCMonitoreo.Controls.Add(this.TbConsulta);
			this.TCMonitoreo.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TCMonitoreo.Location = new System.Drawing.Point(19, 2);
			this.TCMonitoreo.Name = "TCMonitoreo";
			this.TCMonitoreo.SelectedIndex = 0;
			this.TCMonitoreo.Size = new System.Drawing.Size(1044, 655);
			this.TCMonitoreo.TabIndex = 0;
			// 
			// TBMonitoreo
			// 
			this.TBMonitoreo.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.TBMonitoreo.Controls.Add(this.BtnFiltro);
			this.TBMonitoreo.Controls.Add(this.TBoxFiltro);
			this.TBMonitoreo.Controls.Add(this.LblFiltro);
			this.TBMonitoreo.Controls.Add(this.LblMostrarDetalles);
			this.TBMonitoreo.Controls.Add(this.CboxMostrarDetalles);
			this.TBMonitoreo.Controls.Add(this.LblMostrar);
			this.TBMonitoreo.Controls.Add(this.CboxMostrar);
			this.TBMonitoreo.Controls.Add(this.dataGridView1);
			this.TBMonitoreo.Controls.Add(this.textBox2);
			this.TBMonitoreo.Controls.Add(this.DGVDetalle);
			this.TBMonitoreo.Controls.Add(this.LblDetalle);
			this.TBMonitoreo.Controls.Add(this.textBox1);
			this.TBMonitoreo.Controls.Add(this.button3);
			this.TBMonitoreo.Controls.Add(this.label6);
			this.TBMonitoreo.Controls.Add(this.label2);
			this.TBMonitoreo.Controls.Add(this.BtnAgregarSolicitud);
			this.TBMonitoreo.Controls.Add(this.label1);
			this.TBMonitoreo.Controls.Add(this.LblReferencia);
			this.TBMonitoreo.Controls.Add(this.CBoxInterface);
			this.TBMonitoreo.Controls.Add(this.LblInterface);
			this.TBMonitoreo.Controls.Add(this.TBoxReferencia);
			this.TBMonitoreo.Controls.Add(this.splitContainer1);
			this.TBMonitoreo.Location = new System.Drawing.Point(4, 24);
			this.TBMonitoreo.Name = "TBMonitoreo";
			this.TBMonitoreo.Padding = new System.Windows.Forms.Padding(3);
			this.TBMonitoreo.Size = new System.Drawing.Size(1036, 627);
			this.TBMonitoreo.TabIndex = 0;
			this.TBMonitoreo.Text = "Monitoreo";
			// 
			// BtnFiltro
			// 
			this.BtnFiltro.Image = ((System.Drawing.Image)(resources.GetObject("BtnFiltro.Image")));
			this.BtnFiltro.Location = new System.Drawing.Point(322, 79);
			this.BtnFiltro.Name = "BtnFiltro";
			this.BtnFiltro.Size = new System.Drawing.Size(27, 23);
			this.BtnFiltro.TabIndex = 37;
			this.BtnFiltro.UseVisualStyleBackColor = true;
			this.BtnFiltro.Click += new System.EventHandler(this.BtnFiltro_Click);
			// 
			// TBoxFiltro
			// 
			this.TBoxFiltro.Location = new System.Drawing.Point(195, 78);
			this.TBoxFiltro.Name = "TBoxFiltro";
			this.TBoxFiltro.Size = new System.Drawing.Size(125, 23);
			this.TBoxFiltro.TabIndex = 36;
			// 
			// LblFiltro
			// 
			this.LblFiltro.AutoSize = true;
			this.LblFiltro.Location = new System.Drawing.Point(127, 82);
			this.LblFiltro.Name = "LblFiltro";
			this.LblFiltro.Size = new System.Drawing.Size(62, 15);
			this.LblFiltro.TabIndex = 35;
			this.LblFiltro.Text = "Buscar Ref";
			// 
			// LblMostrarDetalles
			// 
			this.LblMostrarDetalles.AutoSize = true;
			this.LblMostrarDetalles.Location = new System.Drawing.Point(912, 82);
			this.LblMostrarDetalles.Name = "LblMostrarDetalles";
			this.LblMostrarDetalles.Size = new System.Drawing.Size(48, 15);
			this.LblMostrarDetalles.TabIndex = 33;
			this.LblMostrarDetalles.Text = "Mostrar";
			this.LblMostrarDetalles.Visible = false;
			// 
			// CboxMostrarDetalles
			// 
			this.CboxMostrarDetalles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CboxMostrarDetalles.FormattingEnabled = true;
			this.CboxMostrarDetalles.Items.AddRange(new object[] {
            "10",
            "20",
            "50",
            "100",
            "200",
            "500",
            "1000",
            "2000"});
			this.CboxMostrarDetalles.Location = new System.Drawing.Point(966, 79);
			this.CboxMostrarDetalles.Name = "CboxMostrarDetalles";
			this.CboxMostrarDetalles.Size = new System.Drawing.Size(57, 23);
			this.CboxMostrarDetalles.TabIndex = 32;
			this.CboxMostrarDetalles.Visible = false;
			this.CboxMostrarDetalles.SelectedIndexChanged += new System.EventHandler(this.CboxMostrarDetalles_SelectedIndexChanged);
			// 
			// LblMostrar
			// 
			this.LblMostrar.AutoSize = true;
			this.LblMostrar.Location = new System.Drawing.Point(394, 82);
			this.LblMostrar.Name = "LblMostrar";
			this.LblMostrar.Size = new System.Drawing.Size(48, 15);
			this.LblMostrar.TabIndex = 31;
			this.LblMostrar.Text = "Mostrar";
			// 
			// CboxMostrar
			// 
			this.CboxMostrar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CboxMostrar.FormattingEnabled = true;
			this.CboxMostrar.Items.AddRange(new object[] {
            "10",
            "20",
            "50",
            "100",
            "200",
            "500",
            "1000",
            "2000"});
			this.CboxMostrar.Location = new System.Drawing.Point(448, 78);
			this.CboxMostrar.Name = "CboxMostrar";
			this.CboxMostrar.Size = new System.Drawing.Size(60, 23);
			this.CboxMostrar.TabIndex = 30;
			this.CboxMostrar.SelectedIndexChanged += new System.EventHandler(this.CboxMostrar_SelectedIndexChanged);
			// 
			// dataGridView1
			// 
			this.dataGridView1.AutoGenerateColumns = false;
			this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11});
			this.dataGridView1.DataSource = this.spRndcGetRequestBindingSource;
			this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dataGridView1.Location = new System.Drawing.Point(17, 108);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(492, 261);
			this.dataGridView1.TabIndex = 16;
			this.dataGridView1.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseClick_1);
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.DataPropertyName = "RequestId";
			this.dataGridViewTextBoxColumn1.HeaderText = "RequestId";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.DataPropertyName = "InterfaceCode";
			this.dataGridViewTextBoxColumn2.HeaderText = "InterfaceCode";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.DataPropertyName = "InterfaceReference";
			this.dataGridViewTextBoxColumn3.HeaderText = "InterfaceReference";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			// 
			// dataGridViewTextBoxColumn4
			// 
			this.dataGridViewTextBoxColumn4.DataPropertyName = "RequestDate";
			this.dataGridViewTextBoxColumn4.HeaderText = "RequestDate";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			// 
			// dataGridViewTextBoxColumn5
			// 
			this.dataGridViewTextBoxColumn5.DataPropertyName = "ProcessDate";
			this.dataGridViewTextBoxColumn5.HeaderText = "ProcessDate";
			this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
			// 
			// dataGridViewTextBoxColumn6
			// 
			this.dataGridViewTextBoxColumn6.DataPropertyName = "Status";
			this.dataGridViewTextBoxColumn6.HeaderText = "Status";
			this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
			// 
			// dataGridViewCheckBoxColumn1
			// 
			this.dataGridViewCheckBoxColumn1.DataPropertyName = "IsSuccessful";
			this.dataGridViewCheckBoxColumn1.HeaderText = "IsSuccessful";
			this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
			// 
			// dataGridViewTextBoxColumn7
			// 
			this.dataGridViewTextBoxColumn7.DataPropertyName = "TransactionId";
			this.dataGridViewTextBoxColumn7.HeaderText = "TransactionId";
			this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
			// 
			// dataGridViewTextBoxColumn8
			// 
			this.dataGridViewTextBoxColumn8.DataPropertyName = "MainErrorCode";
			this.dataGridViewTextBoxColumn8.HeaderText = "MainErrorCode";
			this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
			// 
			// dataGridViewTextBoxColumn9
			// 
			this.dataGridViewTextBoxColumn9.DataPropertyName = "MainErrorDescription";
			this.dataGridViewTextBoxColumn9.HeaderText = "MainErrorDescription";
			this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
			// 
			// dataGridViewTextBoxColumn10
			// 
			this.dataGridViewTextBoxColumn10.DataPropertyName = "User";
			this.dataGridViewTextBoxColumn10.HeaderText = "User";
			this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
			// 
			// dataGridViewTextBoxColumn11
			// 
			this.dataGridViewTextBoxColumn11.DataPropertyName = "NextId";
			this.dataGridViewTextBoxColumn11.HeaderText = "NextId";
			this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
			// 
			// spRndcGetRequestBindingSource
			// 
			this.spRndcGetRequestBindingSource.DataMember = "Sp_Rndc_GetRequest";
			this.spRndcGetRequestBindingSource.DataSource = this.sPX_RNDC_PRODDataSetRequest;
			// 
			// sPX_RNDC_PRODDataSetRequest
			// 
			this.sPX_RNDC_PRODDataSetRequest.DataSetName = "SPX_RNDC_PRODDataSetRequest";
			this.sPX_RNDC_PRODDataSetRequest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(541, 401);
			this.textBox2.Multiline = true;
			this.textBox2.Name = "textBox2";
			this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textBox2.Size = new System.Drawing.Size(482, 190);
			this.textBox2.TabIndex = 20;
			this.textBox2.WordWrap = false;
			// 
			// DGVDetalle
			// 
			this.DGVDetalle.AutoGenerateColumns = false;
			this.DGVDetalle.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
			this.DGVDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.DGVDetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22});
			this.DGVDetalle.DataSource = this.requestDetailBindingSource1;
			this.DGVDetalle.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.DGVDetalle.Location = new System.Drawing.Point(541, 109);
			this.DGVDetalle.Name = "DGVDetalle";
			this.DGVDetalle.Size = new System.Drawing.Size(482, 261);
			this.DGVDetalle.TabIndex = 17;
			this.DGVDetalle.Visible = false;
			this.DGVDetalle.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView2_RowHeaderMouseClick);
			// 
			// dataGridViewTextBoxColumn12
			// 
			this.dataGridViewTextBoxColumn12.DataPropertyName = "DetailId";
			this.dataGridViewTextBoxColumn12.HeaderText = "DetailId";
			this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
			this.dataGridViewTextBoxColumn12.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn13
			// 
			this.dataGridViewTextBoxColumn13.DataPropertyName = "Req_RequestId";
			this.dataGridViewTextBoxColumn13.HeaderText = "Req_RequestId";
			this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
			// 
			// dataGridViewTextBoxColumn14
			// 
			this.dataGridViewTextBoxColumn14.DataPropertyName = "ProcessDate";
			this.dataGridViewTextBoxColumn14.HeaderText = "ProcessDate";
			this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
			// 
			// dataGridViewTextBoxColumn15
			// 
			this.dataGridViewTextBoxColumn15.DataPropertyName = "ProcessId";
			this.dataGridViewTextBoxColumn15.HeaderText = "ProcessId";
			this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
			// 
			// dataGridViewTextBoxColumn16
			// 
			this.dataGridViewTextBoxColumn16.DataPropertyName = "ActionId";
			this.dataGridViewTextBoxColumn16.HeaderText = "ActionId";
			this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
			// 
			// dataGridViewTextBoxColumn17
			// 
			this.dataGridViewTextBoxColumn17.DataPropertyName = "Reference";
			this.dataGridViewTextBoxColumn17.HeaderText = "Reference";
			this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
			// 
			// dataGridViewCheckBoxColumn2
			// 
			this.dataGridViewCheckBoxColumn2.DataPropertyName = "IsSuccessful";
			this.dataGridViewCheckBoxColumn2.HeaderText = "IsSuccessful";
			this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
			// 
			// dataGridViewTextBoxColumn18
			// 
			this.dataGridViewTextBoxColumn18.DataPropertyName = "SoapRequest";
			this.dataGridViewTextBoxColumn18.HeaderText = "SoapRequest";
			this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
			// 
			// dataGridViewTextBoxColumn19
			// 
			this.dataGridViewTextBoxColumn19.DataPropertyName = "SoapResponse";
			this.dataGridViewTextBoxColumn19.HeaderText = "SoapResponse";
			this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
			// 
			// dataGridViewTextBoxColumn20
			// 
			this.dataGridViewTextBoxColumn20.DataPropertyName = "TransactionId";
			this.dataGridViewTextBoxColumn20.HeaderText = "TransactionId";
			this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
			// 
			// dataGridViewTextBoxColumn21
			// 
			this.dataGridViewTextBoxColumn21.DataPropertyName = "ErrorCode";
			this.dataGridViewTextBoxColumn21.HeaderText = "ErrorCode";
			this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
			// 
			// dataGridViewTextBoxColumn22
			// 
			this.dataGridViewTextBoxColumn22.DataPropertyName = "ErrorDescription";
			this.dataGridViewTextBoxColumn22.HeaderText = "ErrorDescription";
			this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
			// 
			// requestDetailBindingSource1
			// 
			this.requestDetailBindingSource1.DataMember = "RequestDetail";
			this.requestDetailBindingSource1.DataSource = this.sPX_RNDC_PRODDataSetDetail;
			// 
			// sPX_RNDC_PRODDataSetDetail
			// 
			this.sPX_RNDC_PRODDataSetDetail.DataSetName = "SPX_RNDC_PRODDataSetDetail";
			this.sPX_RNDC_PRODDataSetDetail.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// LblDetalle
			// 
			this.LblDetalle.AutoSize = true;
			this.LblDetalle.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblDetalle.Location = new System.Drawing.Point(538, 91);
			this.LblDetalle.Name = "LblDetalle";
			this.LblDetalle.Size = new System.Drawing.Size(44, 15);
			this.LblDetalle.TabIndex = 29;
			this.LblDetalle.Text = "Detalle";
			this.LblDetalle.Visible = false;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(17, 401);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textBox1.Size = new System.Drawing.Size(492, 190);
			this.textBox1.TabIndex = 18;
			this.textBox1.WordWrap = false;
			// 
			// button3
			// 
			this.button3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button3.Image = global::Simplexity.AsTrans.RNDC.UI.Management.Properties.Resources.refresh;
			this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button3.Location = new System.Drawing.Point(474, 17);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(126, 35);
			this.button3.TabIndex = 27;
			this.button3.Text = "Actualizar";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.BtnActualizar);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(16, 91);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(54, 15);
			this.label6.TabIndex = 28;
			this.label6.Text = "Solicitud";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(734, 383);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(87, 15);
			this.label2.TabIndex = 21;
			this.label2.Text = "XML Respuesta";
			// 
			// BtnAgregarSolicitud
			// 
			this.BtnAgregarSolicitud.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtnAgregarSolicitud.Image = global::Simplexity.AsTrans.RNDC.UI.Management.Properties.Resources.icon_add;
			this.BtnAgregarSolicitud.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.BtnAgregarSolicitud.Location = new System.Drawing.Point(315, 16);
			this.BtnAgregarSolicitud.Name = "BtnAgregarSolicitud";
			this.BtnAgregarSolicitud.Size = new System.Drawing.Size(140, 36);
			this.BtnAgregarSolicitud.TabIndex = 26;
			this.BtnAgregarSolicitud.Text = "Agregar Solicitud";
			this.BtnAgregarSolicitud.UseVisualStyleBackColor = true;
			this.BtnAgregarSolicitud.Click += new System.EventHandler(this.BtnAgregarSolicitud_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(196, 383);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(117, 15);
			this.label1.TabIndex = 19;
			this.label1.Text = "XML para la solicitud";
			// 
			// LblReferencia
			// 
			this.LblReferencia.AutoSize = true;
			this.LblReferencia.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblReferencia.Location = new System.Drawing.Point(16, 10);
			this.LblReferencia.Name = "LblReferencia";
			this.LblReferencia.Size = new System.Drawing.Size(62, 15);
			this.LblReferencia.TabIndex = 23;
			this.LblReferencia.Text = "Referencia";
			// 
			// CBoxInterface
			// 
			this.CBoxInterface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CBoxInterface.FormattingEnabled = true;
			this.CBoxInterface.Items.AddRange(new object[] {
            "REG-OCA",
            "REG-TER",
            "REG-VEH",
            "REG-MAN",
            "REG-CUM",
            "REG-DIR",
            "ANU-REM",
            "ANU-MAN"});
			this.CBoxInterface.Location = new System.Drawing.Point(165, 25);
			this.CBoxInterface.Name = "CBoxInterface";
			this.CBoxInterface.Size = new System.Drawing.Size(117, 23);
			this.CBoxInterface.TabIndex = 24;
			// 
			// LblInterface
			// 
			this.LblInterface.AutoSize = true;
			this.LblInterface.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblInterface.Location = new System.Drawing.Point(162, 9);
			this.LblInterface.Name = "LblInterface";
			this.LblInterface.Size = new System.Drawing.Size(54, 15);
			this.LblInterface.TabIndex = 25;
			this.LblInterface.Text = "Interface";
			// 
			// TBoxReferencia
			// 
			this.TBoxReferencia.Location = new System.Drawing.Point(19, 25);
			this.TBoxReferencia.Name = "TBoxReferencia";
			this.TBoxReferencia.Size = new System.Drawing.Size(134, 23);
			this.TBoxReferencia.TabIndex = 22;
			// 
			// splitContainer1
			// 
			this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(3, 3);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
			this.splitContainer1.Panel1.Controls.Add(this.LblRTotal);
			this.splitContainer1.Panel1.Controls.Add(this.LblRProcesadas);
			this.splitContainer1.Panel1.Controls.Add(this.LblRPendientes);
			this.splitContainer1.Panel1.Controls.Add(this.LblTotal);
			this.splitContainer1.Panel1.Controls.Add(this.LblProcesadas);
			this.splitContainer1.Panel1.Controls.Add(this.LblPendientes);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.LblVersion);
			this.splitContainer1.Size = new System.Drawing.Size(1030, 621);
			this.splitContainer1.SplitterDistance = 63;
			this.splitContainer1.TabIndex = 38;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(868, 1);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(161, 57);
			this.pictureBox1.TabIndex = 6;
			this.pictureBox1.TabStop = false;
			// 
			// LblRTotal
			// 
			this.LblRTotal.AutoSize = true;
			this.LblRTotal.Location = new System.Drawing.Point(787, 41);
			this.LblRTotal.Name = "LblRTotal";
			this.LblRTotal.Size = new System.Drawing.Size(14, 15);
			this.LblRTotal.TabIndex = 5;
			this.LblRTotal.Text = "0";
			// 
			// LblRProcesadas
			// 
			this.LblRProcesadas.AutoSize = true;
			this.LblRProcesadas.Location = new System.Drawing.Point(787, 23);
			this.LblRProcesadas.Name = "LblRProcesadas";
			this.LblRProcesadas.Size = new System.Drawing.Size(14, 15);
			this.LblRProcesadas.TabIndex = 4;
			this.LblRProcesadas.Text = "0";
			// 
			// LblRPendientes
			// 
			this.LblRPendientes.AutoSize = true;
			this.LblRPendientes.Location = new System.Drawing.Point(787, 5);
			this.LblRPendientes.Name = "LblRPendientes";
			this.LblRPendientes.Size = new System.Drawing.Size(14, 15);
			this.LblRPendientes.TabIndex = 3;
			this.LblRPendientes.Text = "0";
			// 
			// LblTotal
			// 
			this.LblTotal.AutoSize = true;
			this.LblTotal.Location = new System.Drawing.Point(743, 41);
			this.LblTotal.Name = "LblTotal";
			this.LblTotal.Size = new System.Drawing.Size(36, 15);
			this.LblTotal.TabIndex = 2;
			this.LblTotal.Text = "Total:";
			// 
			// LblProcesadas
			// 
			this.LblProcesadas.AutoSize = true;
			this.LblProcesadas.Location = new System.Drawing.Point(711, 23);
			this.LblProcesadas.Name = "LblProcesadas";
			this.LblProcesadas.Size = new System.Drawing.Size(69, 15);
			this.LblProcesadas.TabIndex = 1;
			this.LblProcesadas.Text = "Procesadas:";
			// 
			// LblPendientes
			// 
			this.LblPendientes.AutoSize = true;
			this.LblPendientes.Location = new System.Drawing.Point(712, 5);
			this.LblPendientes.Name = "LblPendientes";
			this.LblPendientes.Size = new System.Drawing.Size(68, 15);
			this.LblPendientes.TabIndex = 0;
			this.LblPendientes.Text = "Pendientes:";
			// 
			// LblVersion
			// 
			this.LblVersion.AutoSize = true;
			this.LblVersion.Location = new System.Drawing.Point(966, 536);
			this.LblVersion.Name = "LblVersion";
			this.LblVersion.Size = new System.Drawing.Size(18, 15);
			this.LblVersion.TabIndex = 0;
			this.LblVersion.Text = "V ";
			// 
			// TbConsulta
			// 
			this.TbConsulta.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.TbConsulta.Controls.Add(this.ChBoxTodos);
			this.TbConsulta.Controls.Add(this.DGVResultados);
			this.TbConsulta.Controls.Add(this.BtnConsultar);
			this.TbConsulta.Controls.Add(this.LblResultados);
			this.TbConsulta.Controls.Add(this.DGVCampos);
			this.TbConsulta.Controls.Add(this.CBoxTipoconsulta);
			this.TbConsulta.Controls.Add(this.lblTipConsulta);
			this.TbConsulta.Controls.Add(this.splitContainer2);
			this.TbConsulta.Location = new System.Drawing.Point(4, 24);
			this.TbConsulta.Name = "TbConsulta";
			this.TbConsulta.Padding = new System.Windows.Forms.Padding(3);
			this.TbConsulta.Size = new System.Drawing.Size(1036, 627);
			this.TbConsulta.TabIndex = 1;
			this.TbConsulta.Text = "Consulta";
			// 
			// ChBoxTodos
			// 
			this.ChBoxTodos.AutoSize = true;
			this.ChBoxTodos.Location = new System.Drawing.Point(10, 86);
			this.ChBoxTodos.Name = "ChBoxTodos";
			this.ChBoxTodos.Size = new System.Drawing.Size(58, 19);
			this.ChBoxTodos.TabIndex = 8;
			this.ChBoxTodos.Text = "Todos";
			this.ChBoxTodos.UseVisualStyleBackColor = true;
			this.ChBoxTodos.Visible = false;
			this.ChBoxTodos.CheckedChanged += new System.EventHandler(this.ChBoxTodos_CheckedChanged);
			// 
			// DGVResultados
			// 
			this.DGVResultados.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
			this.DGVResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.DGVResultados.Location = new System.Drawing.Point(13, 347);
			this.DGVResultados.Name = "DGVResultados";
			this.DGVResultados.Size = new System.Drawing.Size(1016, 245);
			this.DGVResultados.TabIndex = 7;
			// 
			// BtnConsultar
			// 
			this.BtnConsultar.Image = ((System.Drawing.Image)(resources.GetObject("BtnConsultar.Image")));
			this.BtnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.BtnConsultar.Location = new System.Drawing.Point(232, 25);
			this.BtnConsultar.Name = "BtnConsultar";
			this.BtnConsultar.Size = new System.Drawing.Size(105, 37);
			this.BtnConsultar.TabIndex = 5;
			this.BtnConsultar.Text = "Consultar";
			this.BtnConsultar.UseVisualStyleBackColor = true;
			this.BtnConsultar.Click += new System.EventHandler(this.BtnConsultar_Click);
			// 
			// LblResultados
			// 
			this.LblResultados.AutoSize = true;
			this.LblResultados.Location = new System.Drawing.Point(10, 329);
			this.LblResultados.Name = "LblResultados";
			this.LblResultados.Size = new System.Drawing.Size(64, 15);
			this.LblResultados.TabIndex = 3;
			this.LblResultados.Text = "Resultados";
			// 
			// DGVCampos
			// 
			this.DGVCampos.AutoGenerateColumns = false;
			this.DGVCampos.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
			this.DGVCampos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.DGVCampos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fieldDescriptionDataGridViewTextBoxColumn,
            this.Consultar,
            this.Criterio});
			this.DGVCampos.DataSource = this.fieldsToQueryBindingSource;
			this.DGVCampos.Location = new System.Drawing.Point(10, 111);
			this.DGVCampos.Name = "DGVCampos";
			this.DGVCampos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.DGVCampos.Size = new System.Drawing.Size(524, 211);
			this.DGVCampos.TabIndex = 2;
			this.DGVCampos.Visible = false;
			// 
			// fieldDescriptionDataGridViewTextBoxColumn
			// 
			this.fieldDescriptionDataGridViewTextBoxColumn.DataPropertyName = "FieldDescription";
			this.fieldDescriptionDataGridViewTextBoxColumn.HeaderText = "Campo";
			this.fieldDescriptionDataGridViewTextBoxColumn.Name = "fieldDescriptionDataGridViewTextBoxColumn";
			this.fieldDescriptionDataGridViewTextBoxColumn.ReadOnly = true;
			this.fieldDescriptionDataGridViewTextBoxColumn.Width = 230;
			// 
			// Consultar
			// 
			this.Consultar.HeaderText = "Consultar";
			this.Consultar.Name = "Consultar";
			// 
			// Criterio
			// 
			this.Criterio.HeaderText = "Criterio";
			this.Criterio.Name = "Criterio";
			this.Criterio.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.Criterio.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.Criterio.Width = 150;
			// 
			// fieldsToQueryBindingSource
			// 
			this.fieldsToQueryBindingSource.DataMember = "FieldsToQuery";
			this.fieldsToQueryBindingSource.DataSource = this.sPX_RNDC_PRODDataSetFieldsToQuery;
			// 
			// sPX_RNDC_PRODDataSetFieldsToQuery
			// 
			this.sPX_RNDC_PRODDataSetFieldsToQuery.DataSetName = "SPX_RNDC_PRODDataSetFieldsToQuery";
			this.sPX_RNDC_PRODDataSetFieldsToQuery.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// CBoxTipoconsulta
			// 
			this.CBoxTipoconsulta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CBoxTipoconsulta.FormattingEnabled = true;
			this.CBoxTipoconsulta.Items.AddRange(new object[] {
            "Vehiculo",
            "Tercero"});
			this.CBoxTipoconsulta.Location = new System.Drawing.Point(10, 33);
			this.CBoxTipoconsulta.Name = "CBoxTipoconsulta";
			this.CBoxTipoconsulta.Size = new System.Drawing.Size(198, 23);
			this.CBoxTipoconsulta.TabIndex = 1;
			this.CBoxTipoconsulta.SelectedIndexChanged += new System.EventHandler(this.CBoxTipoconsulta_SelectedIndexChanged);
			// 
			// lblTipConsulta
			// 
			this.lblTipConsulta.AutoSize = true;
			this.lblTipConsulta.Location = new System.Drawing.Point(7, 13);
			this.lblTipConsulta.Name = "lblTipConsulta";
			this.lblTipConsulta.Size = new System.Drawing.Size(96, 15);
			this.lblTipConsulta.TabIndex = 0;
			this.lblTipConsulta.Text = "Tipo de Consulta";
			// 
			// splitContainer2
			// 
			this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(3, 3);
			this.splitContainer2.Name = "splitContainer2";
			this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.pictureBox2);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.LblVersion2);
			this.splitContainer2.Size = new System.Drawing.Size(1030, 621);
			this.splitContainer2.SplitterDistance = 62;
			this.splitContainer2.TabIndex = 9;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(867, 0);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(162, 58);
			this.pictureBox2.TabIndex = 0;
			this.pictureBox2.TabStop = false;
			// 
			// LblVersion2
			// 
			this.LblVersion2.AutoSize = true;
			this.LblVersion2.Location = new System.Drawing.Point(963, 537);
			this.LblVersion2.Name = "LblVersion2";
			this.LblVersion2.Size = new System.Drawing.Size(18, 15);
			this.LblVersion2.TabIndex = 1;
			this.LblVersion2.Text = "V ";
			// 
			// fieldsToQueryTableAdapter
			// 
			this.fieldsToQueryTableAdapter.ClearBeforeFill = true;
			// 
			// requestDetailTableAdapter
			// 
			this.requestDetailTableAdapter.ClearBeforeFill = true;
			// 
			// sp_Rndc_GetRequestTableAdapter
			// 
			this.sp_Rndc_GetRequestTableAdapter.ClearBeforeFill = true;
			// 
			// Management
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.BackColor = System.Drawing.SystemColors.HighlightText;
			this.ClientSize = new System.Drawing.Size(1046, 656);
			this.Controls.Add(this.TCMonitoreo);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximumSize = new System.Drawing.Size(1062, 695);
			this.MinimumSize = new System.Drawing.Size(1062, 695);
			this.Name = "Management";
			this.Text = "Modulo de Monitoreo";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.TCMonitoreo.ResumeLayout(false);
			this.TBMonitoreo.ResumeLayout(false);
			this.TBMonitoreo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spRndcGetRequestBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.sPX_RNDC_PRODDataSetRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DGVDetalle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.requestDetailBindingSource1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.sPX_RNDC_PRODDataSetDetail)).EndInit();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.PerformLayout();
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.TbConsulta.ResumeLayout(false);
			this.TbConsulta.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.DGVResultados)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DGVCampos)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fieldsToQueryBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.sPX_RNDC_PRODDataSetFieldsToQuery)).EndInit();
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			this.splitContainer2.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridViewTextBoxColumn detailIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn reqRequestIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn processDateDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn processIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn actionIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn referenceDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn isSuccessfulDataGridViewCheckBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn soapRequestDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn soapResponseDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn transactionIdDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn errorCodeDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn errorDescriptionDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn requestIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn interfaceCodeDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn interfaceReferenceDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn requestDateDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn processDateDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn isSuccessfulDataGridViewCheckBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn transactionIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn mainErrorCodeDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn mainErrorDescriptionDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn userDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn nextIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.TabControl TCMonitoreo;
    private System.Windows.Forms.TabPage TBMonitoreo;
    private System.Windows.Forms.DataGridView dataGridView1;
    private System.Windows.Forms.TextBox textBox2;
    private System.Windows.Forms.DataGridView DGVDetalle;
    private System.Windows.Forms.Label LblDetalle;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button BtnAgregarSolicitud;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label LblReferencia;
    private System.Windows.Forms.ComboBox CBoxInterface;
    private System.Windows.Forms.Label LblInterface;
    private System.Windows.Forms.TextBox TBoxReferencia;
    private System.Windows.Forms.TabPage TbConsulta;
    private System.Windows.Forms.DataGridView DGVCampos;
    private System.Windows.Forms.ComboBox CBoxTipoconsulta;
    private System.Windows.Forms.Label lblTipConsulta;
    private SPX_RNDC_PRODDataSetRequest sPX_RNDC_PRODDataSetRequest;
    private System.Windows.Forms.BindingSource spRndcGetRequestBindingSource;
    private SPX_RNDC_PRODDataSetRequestTableAdapters.Sp_Rndc_GetRequestTableAdapter sp_Rndc_GetRequestTableAdapter;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
    private SPX_RNDC_PRODDataSetDetail sPX_RNDC_PRODDataSetDetail;
    private System.Windows.Forms.BindingSource requestDetailBindingSource1;
    private SPX_RNDC_PRODDataSetDetailTableAdapters.RequestDetailTableAdapter requestDetailTableAdapter;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
    private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
    private SPX_RNDC_PRODDataSetFieldsToQuery sPX_RNDC_PRODDataSetFieldsToQuery;
    private System.Windows.Forms.BindingSource fieldsToQueryBindingSource;
    private SPX_RNDC_PRODDataSetFieldsToQueryTableAdapters.FieldsToQueryTableAdapter fieldsToQueryTableAdapter;
    private System.Windows.Forms.Label LblResultados;
    private System.Windows.Forms.Button BtnConsultar;
    private System.Windows.Forms.DataGridViewTextBoxColumn fieldDescriptionDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn Consultar;
    private System.Windows.Forms.DataGridViewTextBoxColumn Criterio;
    private System.Windows.Forms.DataGridView DGVResultados;
    private System.Windows.Forms.CheckBox ChBoxTodos;
    private System.Windows.Forms.Label LblMostrarDetalles;
    private System.Windows.Forms.ComboBox CboxMostrarDetalles;
    private System.Windows.Forms.Label LblMostrar;
    private System.Windows.Forms.ComboBox CboxMostrar;
    private System.Windows.Forms.Label LblFiltro;
    private System.Windows.Forms.Button BtnFiltro;
    private System.Windows.Forms.TextBox TBoxFiltro;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.Label LblTotal;
    private System.Windows.Forms.Label LblProcesadas;
    private System.Windows.Forms.Label LblPendientes;
    private System.Windows.Forms.Label LblRTotal;
    private System.Windows.Forms.Label LblRProcesadas;
    private System.Windows.Forms.Label LblRPendientes;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.PictureBox pictureBox2;
    private System.Windows.Forms.Label LblVersion;
    private System.Windows.Forms.Label LblVersion2;
  }
}

