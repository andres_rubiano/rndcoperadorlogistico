﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Timers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Services;

namespace Simplexity.AsTrans.RNDC
{
    public class RndcService
    {
        private Timer _timer;
        private string _stopReason = string.Empty;

        public void OnStart()
        {
            StartService();
        }

        private static void ProcessRequests()
        {
            try
            {
                var requestServices = new RequestServices();
                requestServices.ProcessRequests();
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
            }
        }

        public void StartService()
        {
            try
            {
                if (ValidateVersion())
                {
                    int interval;
                    int.TryParse(ConfigurationManager.AppSettings["INTERVAL"], out interval);
                    _timer = new Timer();
                    _timer.Elapsed += TimerElapsed;
                    _timer.Interval = interval;
                    _timer.Enabled = true;
                    _timer.AutoReset = true;

                }
                else
                {
                    _stopReason = Resources.ReasonStopService;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
            }
        }

        public void OnStop()
        {
            _timer.Enabled = false;
            Logger.Write(String.Format("The service rndc has been stopped " + _stopReason), "General", 0, 777,
                TraceEventType.Information, "RndcServiceInfo");
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Enabled = false;
            try
            {
                ProcessRequests();
            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
            }
            _timer.Enabled = true;
        }

        private static bool ValidateVersion()
        {
            var requestServices = new RequestServices();
            bool response = requestServices.GetVersion();
            return response;
        }
    }
}