﻿using System.Configuration;
using Topshelf;

namespace Simplexity.AsTrans.RNDC
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var servicioNombre = ConfigurationManager.AppSettings["ServicioNombre"];

            HostFactory.Run(x =>
            {
                x.Service<RndcService>(s =>
                {
                    s.ConstructUsing(name => new RndcService());
                    s.WhenStarted(tc => tc.OnStart());
                    s.WhenStopped(tc => tc.OnStop());
                });
                x.StartAutomaticallyDelayed();
                x.RunAsLocalSystem();
                x.SetDescription("Servicio que se encarga de gestionar solicitudes que se envian al Web Service Rndc.");
                x.SetDisplayName(servicioNombre);
                x.SetServiceName(servicioNombre);
            });
        }
    }
}