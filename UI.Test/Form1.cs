﻿using System;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Services;

namespace Simplexity.AsTrans.RNDC.UI.Test
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				RequestServices requestServices = new RequestServices();
				requestServices.ProcessRequests();
			}
			catch (Exception ex)
			{
				ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
				;
			}
		}

		private void GenerarQr_Click(object sender, EventArgs e)
		{
      // Manifiesto invalido:

      //ManifestQrInformationDto info = new ManifestQrInformationDto();
      //info.ManifiestoNumero = "001234888";
      //info.Mec = @"29256377";
      //info.Fecha = @"19/02/2018";
      //info.Placa = @"BOD874";
      //info.Remolque = @"";
      //info.CiudadOrigen = @"BOGOTA BOGOTA D. C.";
      //info.CiudadDestino = @"CALI VALLE DEL CAUCA";
      //info.Mercancia = @"PRUEBA";
      //info.ConductorId = @"19258361";
      //info.Empresa = @"INTER TRANSPORTE DE MERCANCIAS";
      //info.ObservacionesRndc = @"";
      //info.CodigoSeguridadQr = @"u9pPY6ImVnLy+Nx7RRvwOsZWcpI=";

      // Manifiesto válido:

      ManifestQrInformationDto info = new ManifestQrInformationDto();
      info.ManifiestoNumero = "29894343";
      info.Mec = @"29894343";
      info.Fecha = @"2018/03/16";
      info.Placa = @"BOD874";
      info.Remolque = @"";
		    info.ConfiguracionConjunto = "2";
      info.CiudadOrigen = @"CUCUTA NORTE DE SANT";
      info.CiudadDestino = @"DIBULLA LA GUAJIRA";
      info.Mercancia = @"CARBÓN.";
      info.ConductorId = @"19258361";
      info.Empresa = @"MINISTERIO DE TRANSPORTE";
      info.ObservacionesRndc = @"Reducción tarifa 50% en Peajes: EL_COPEY, LA_LOMA, TUCURINCA, PAILITAS, PLATANAL, GAMARRA.  Resolución 540 y 541 de 2018";
      info.CodigoSeguridadQr = @"mGfbvdFgdtVMA+UqPfzt9AlbTD0=";

			string demoUrl, demoLocalPath;
			BusinessRules.Main.QrCodeGenerator.Create(info, out demoUrl, out demoLocalPath);
		}
	}
}