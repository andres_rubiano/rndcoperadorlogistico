﻿namespace Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs
{
	public class ManifestQrInformationDto
	{
		public string ManifiestoNumero { get; set; }
		public string Mec { get; set; }
		public string Fecha { get; set; }
		public string Placa { get; set; }
		public string Remolque { get; set; }
    public string ConfiguracionConjunto { get; set; }
		public string CiudadOrigen { get; set; }
		public string CiudadDestino { get; set; }
		public string Mercancia { get; set; }
		public string ConductorId { get; set; }
		public string Empresa { get; set; }
    public string CodigoSeguridadQr { get; set; }
		public string ObservacionesRndc { get; set; }
	}
}