﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs
{
  public class RequestDetailDTO
  {
    public int DetailId { get; set; }
    public int RequestId { get; set; }
    public DateTime ProcessDate { get; set; }
    public int Processid { get; set; }
    public int ActionId { get; set; }
    public string Reference { get; set; }
    public bool Sucessful { get; set; }
    public string SoapOut { get; set; }
    public string SoapIn { get; set; }
    public string TransactionId { get; set; }
    public string ErrorCode { get; set; }
    public string ErrorDescription { get; set; }
  }
}
