﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs
{
  public class RequestDTO
  {
    public int RequestId { get; set; }
    public string InterfaceCode { get; set; }
    public string InterfaceReference { get; set; }
    public DateTime RequestDate { get; set; }
    public DateTime ProcessDate { get; set; }
    public int Status { get; set; }
    public bool Sucessful { get; set; }
    public string TransactionId { get; set; }
    public string MainErrorCode { get; set; }
    public string MainErrorDescription { get; set; }
    public string User { get; set; }
    public int NextId { get; set; }
    //public List<RequestDetailDTO> Details { get; set; }

      public void SetSucessful(string transactionId)
      {
          Sucessful = true;
          TransactionId = transactionId;
      }
  }
}
