﻿using System;
using Common;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    public class Shipment
    {
        public void ProcessShipmentCorrection(RequestDTO requestDTO)
        {
            var transactions = new Transactions();
            var data = new Data();
            var processId = (int)RequestType.ProcessId.CorregirRemesa;
            var actionId = (int)RequestType.ActionId.Registrar;

            var requestDetail = new RequestDetailDTO
                                  {
                                      RequestId = requestDTO.RequestId,
                                      ActionId = actionId,
                                      Processid = processId,
                                      Reference = requestDTO.InterfaceReference


                                  };

            if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.Remesa))
            {
                //InterfaceSuccesful = false;
                requestDetail.ProcessDate = System.DateTime.Now;
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription,
                                     IsSucessful.IsNotSucessful, Resources.RemesaRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
            }
            else
            {
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.Sucessful, Resources.RemesaRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
                requestDTO.Sucessful = true;
                requestDTO.TransactionId = requestDetail.TransactionId;

            }
            data.UpdateRequest(requestDTO);

        }

        /// <summary>
        /// Procesa el cumplido inicial de la remesa (nueva interfaz Nov 2018)
        /// </summary>
        /// <param name="requestDTO"></param>
        public void ProcesarCumplidoInicialRemesa(RequestDTO requestDTO)
        {
            var transactions = new Transactions();
            var data = new Data();
            var processId = (int)RequestType.ProcessId.CumplidoInicialRemesa;
            var actionId = (int)RequestType.ActionId.Registrar;

            var requestDetail = new RequestDetailDTO
            {
                RequestId = requestDTO.RequestId,
                ActionId = actionId,
                Processid = processId,
                Reference = requestDTO.InterfaceReference
            };

            if (!transactions.ObtenerXmlConsumirWsRndc(requestDTO, requestDetail, Resources.spRemesaCumplidoInicialObtenerXML))
            {
                //InterfaceSuccesful = false;
                requestDetail.ProcessDate = System.DateTime.Now;
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription,
                                     IsSucessful.IsNotSucessful, Resources.spRemesaCumplidoInicialRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
            }
            else
            {
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.Sucessful, Resources.spRemesaCumplidoInicialRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
                requestDTO.Sucessful = true;
                requestDTO.TransactionId = requestDetail.TransactionId;

            }
            data.UpdateRequest(requestDTO);

        }
    }
}


