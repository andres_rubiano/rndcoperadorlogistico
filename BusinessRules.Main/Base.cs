﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using Common;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.co.gov.mintransporte.rndc;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.RndcMockUpService;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    public class Base
    {
        protected string User { get; set; }
        protected string Password { get; set; }
        public string ServiceUrl { get; set; }
        public bool MockUpRndcActive { get; set; }
        public string MockUpRndcServiceUrl { get; set; }
        private int TimeOut { get; set; }
        protected LogRecord<List<string>> LogRecordString { get; set; } 

        public Base()
        {
            User = ConfigurationManager.AppSettings["USER"];
            Password = ConfigurationManager.AppSettings["PASSWORD"];
            ServiceUrl = ConfigurationManager.AppSettings["SERVICEURL"];

            bool mockUpRndcActive;
            bool.TryParse(ConfigurationManager.AppSettings["MockUpRndcActive"], out mockUpRndcActive);
            MockUpRndcActive = mockUpRndcActive;

            MockUpRndcServiceUrl = ConfigurationManager.AppSettings["MockUpRndcServiceUrl"];

            int timeOut;
            int.TryParse(ConfigurationManager.AppSettings["WSRESPONSETIMEOUT"], out timeOut);
            TimeOut = timeOut;

            LogRecordString = new LogRecord<List<string>>(new List<string>());
        }

        private string RndcMockUpAtenderMensaje(string xmlRequest)
        {
            var myBinding = new BasicHttpBinding();
            myBinding.Security.Mode = BasicHttpSecurityMode.None;
            var myEndpointAddress = new EndpointAddress(MockUpRndcServiceUrl);

            var rndcMockUp = new ServiceRndcMockUpClient(myBinding, myEndpointAddress);

            return rndcMockUp.AtenderMensajeRNDC(xmlRequest);
        }

        private string RndcAtenderMensaje(string xmlRequest)
        {
            var ibpm = new IBPMServicesservice { Timeout = TimeOut, Url = ServiceUrl };
            var respuesta = ibpm.AtenderMensajeRNDC(xmlRequest);

            var lista = new List<string>();
            lista.Add(xmlRequest);
            lista.Add(respuesta);
            LogRecordString.WriteLog(lista, "LogRndcAtenderMensajeRNDC");

            return respuesta;
        }

        public string AtenderMensajeRndc(string xmlText)
        {
            return MockUpRndcActive ? RndcMockUpAtenderMensaje(xmlText) : RndcAtenderMensaje(xmlText);
        }
    }
}