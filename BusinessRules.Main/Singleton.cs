﻿namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
  public class Singleton
  {
    public static Singleton Instancia = new Singleton();
    public string AstransInterface { get; set; }
    private Singleton(){}
  }
}
