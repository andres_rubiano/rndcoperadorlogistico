﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.Infrastructure.Data.Main.EF;


namespace Simplexity.AsTrans.RNDC.BusinessRules.Main.Services
{
    public class RequestServices
    {
        public void ProcessRequests()
        {
            // 1. Consulta en la DB los request pendientes de procesar
            bool traceability;
            bool.TryParse(ConfigurationManager.AppSettings["TRACEABILITY"], out traceability);
            var data = new Data();

            if (traceability)
                InfoLog(Resources.GetPendingRequest);
            var pendingRequests = GetPendingRequest();
            if (pendingRequests == null || pendingRequests.Count <= 0) return;

            Console.WriteLine(@"Registros a procesar: {0}", pendingRequests.Count());

            foreach (var request in pendingRequests)
            {
                try
                {
                    #region processRequest

                    Console.WriteLine(@"    Procesando [{0}] referencia [{1}]", request.InterfaceCode,
                        request.InterfaceReference);
                    Singleton.Instancia.AstransInterface = request.InterfaceCode;
                    if (!VerifyRunningStatus())
                    {
                        InfoLog(Resources.SafeStop);
                        break;
                    }
                    int timeOut;
                    int.TryParse(ConfigurationManager.AppSettings["WAITBETWEENREQUETS"], out timeOut);
                    Thread.Sleep(timeOut);
                    request.InterfaceCode = request.InterfaceCode.Trim();
                    switch (request.InterfaceCode)
                    {
                        case Interfaces.RegisterThirdParty:
                            if (traceability)
                                InfoLog(Resources.RegisterThirdParty);
                            new ThirdParty().ProcessRegisterThirdParty(request);
                            break;
                        case Interfaces.RegisterVehicle:
                            if (traceability)
                                InfoLog(Resources.RegisterVehicle);
                            new Vehicle().ProcessRegisterVehicle(request);
                            break;
                        case Interfaces.RegisterLoadingOrder:
                            if (traceability)
                                InfoLog(Resources.RegisterLoadingOrder);
                            new OrderCharge().ReportLoadingOrderProcess(request);
                            break;
                        case Interfaces.RegisterrManifest:
                            if (traceability)
                                InfoLog(Resources.RegisterrManifest);
                            new Manifest().ProcessRegisterManifest(request);
                            break;
                        case Interfaces.FulfillManifest:
                            if (traceability)
                                InfoLog(Resources.FulfillManifest);
                            new FulfillManifest().ProcessFulfillManifest(request);
                            break;
                        case Interfaces.RegisterLocations:
                            if (traceability)
                                InfoLog(Resources.RegisterLocations);
                            new RegisterLocations().ProcessRegisterLocations(request);
                            break;
                        case Interfaces.CorrectShipment:
                            if (traceability)
                                InfoLog(Resources.CorregirRemesa);
                            new Shipment().ProcessShipmentCorrection(request);
                            break;
                        case Interfaces.CancelRem:
                            if (traceability)
                                InfoLog(Resources.CancelRem);
                            new Cancellations().CancelRem(request);
                            break;
                        case Interfaces.CancelManifest:
                            if (traceability)
                                InfoLog(Resources.CancelManifest);
                            new Cancellations().CancelManifest(request);
                            break;
                            //Registrar Cumplido Inicial de Remesa
                        case Interfaces.RegistrarCumplidoInicialRemesa:
                            if (traceability)
                                InfoLog(Resources.LabelCumplidoInicialRemesa);
                            new Shipment().ProcesarCumplidoInicialRemesa(request);
                            break;

                            //Registrar Anulación del Cumplido Inicial de Remesa
                        case Interfaces.AnularCumplidoInicialRemesa:
                            if (traceability)
                                InfoLog(Resources.LabelAnularCumplidoInicialRemesa);
                            new Cancellations().CancelarCumplidoInicialRemesa(request);
                            break;

                        case Interfaces.ConsultarFirmaElectronicaManifiesto:
                            if (traceability)
                                InfoLog(Resources.ConsultarFirmaElectronicaManifiesto);
                            new Manifest().GetElectronicSignature(request);
                            break;

                    }
                    Console.WriteLine(@"    [Finalizado]");

                    #endregion
                }
                catch (TimeoutException timeoutException)
                {
                    data.UpdateRequestWithError(request, timeoutException.Message, true);
                    Console.WriteLine(@"    [Finalizado con novedad de TimeOut]"); 
                }
                catch (Exception ex)
                {
                    if (ex.Message.Equals(@"The operation has timed out"))
                    {
                        data.UpdateRequestWithError(request, string.Format("{0}_{1}",ex.Message, ex.GetType()), true);
                        Console.WriteLine(@"    [Finalizado con novedad (ex) de TimeOut]"); 
                    }

                    data.UpdateRequestWithError(request, ex.Message, false);
                    Console.WriteLine(@"    [Finalizado con novedad]");
                }
                finally
                {

                }
            }
        }

        private List<RequestDTO> GetPendingRequest()
        {
            using (var requestEntities = new SPX_RNDC_PRODEntities())
            {
                // para pruebas: && p.InterfaceCode == "REG-TER" && p.InterfaceReference == "1065571056"

                var query = (from p in requestEntities.Request
                             where p.Status == (int)OperationConstants.Status.NonProcess
                             orderby p.RequestId ascending
                             select new RequestDTO
                             {
                                 RequestId = p.RequestId,
                                 InterfaceCode = p.InterfaceCode ?? "0",
                                 InterfaceReference = p.InterfaceReference ?? "0",
                                 RequestDate = p.RequestDate ?? System.DateTime.Now

                             }).Take(100).ToList();

                return query;
            }

        }

        public static void InfoLog(string info)
        {
            Logger.Write(info, "General", 0, 777, TraceEventType.Information, "RndcServiceInfo");
        }

        public bool GetVersion()
        {
            string version = System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();
            string parameter = ConfigurationManager.AppSettings["PARAMETER"];
            var data = new Data();
            return data.GetParameter(parameter, version);
        }

        private static bool VerifyRunningStatus()
        {
            var data = new Data();
            return data.GetRunningStatus();
        }
    }
}
