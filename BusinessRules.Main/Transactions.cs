﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.co.gov.mintransporte.rndc;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    public class Transactions : Base
    {
        public bool TransactDetail(RequestDTO requestDto, RequestDetailDTO requestDetail, string storeProcedure)
        {
            //requestDetail debe tener la referencia, el ID, el actionID y el processID
            //limpiar objeto
            var data = new Data();
            var responseTransaction = false;
            requestDetail.Reference = requestDetail.Reference.Trim();
            var thereError = false;
            //linea original de proceso 
            //var values = data.GetInformationAstrans(requestDetail.Reference, storeProcedure, ref thereError);

            // cambio 11/17/2015 para adicionar cor-rem codigo 38 al sp Sp_Rndc_RemesaObtenerXML 
            var values = data.GetInformationAstrans(requestDetail.Reference, storeProcedure, ref thereError, requestDetail.Processid);
            var templateXml = Resources.Request;

            if (!thereError)
            {
                if (!String.IsNullOrEmpty(values))
                {
                    templateXml = string.Format(templateXml, User, Password, requestDetail.ActionId,
                                       requestDetail.Processid, values);

                    var serviceResponse = AtenderMensajeRndc(templateXml);

                    //var lista = new List<string>();
                    //lista.Add(templateXml);
                    //lista.Add(serviceResponse);
                    //LogRecordString.WriteLog(lista, string.Format("request_{0}_{1}", requestDto.InterfaceCode, requestDto.InterfaceReference));

                    requestDetail.SoapIn = templateXml;
                    requestDetail.SoapOut = serviceResponse;
                    requestDetail.ProcessDate = DateTime.Now;

                    var response = new Response();
                    var id = false;
                    var reply = response.DecodeReply(serviceResponse, ref id);


                    if (id)
                    {
                        requestDetail.Sucessful = true;
                        requestDetail.TransactionId = reply;
                        responseTransaction = true;
                    }
                    else
                    {
                        requestDetail.Sucessful = false;
                        requestDetail.ErrorDescription = reply + string.Format(Resources.ErrorReference, requestDetail.Reference);
                        requestDto.Sucessful = false;
                        requestDto.ProcessDate = System.DateTime.Now;
                        requestDto.MainErrorDescription = reply + string.Format(Resources.ErrorReference, requestDetail.Reference);
                    }
                }
                else
                {
                    requestDetail.Sucessful = false;
                    requestDetail.ErrorDescription = string.Format(Resources.InformationMessage, storeProcedure, requestDetail.Reference);
                    requestDto.Sucessful = false;
                    requestDto.ProcessDate = System.DateTime.Now.ToLocalTime();
                    requestDto.MainErrorDescription = string.Format(Resources.InformationMessage, storeProcedure, requestDetail.Reference);
                }
            }
            else
            {
                requestDetail.Sucessful = false;
                requestDetail.ErrorDescription = values;
                requestDto.Sucessful = false;
                requestDto.MainErrorDescription = values;
            }
            return responseTransaction;
        }

        /// <summary>
        /// Este método toma una referencia de tarea, un SP y obtiene la data
        /// contume el WS del ministerio y retorna los request y requestDetail para que agua arriba
        /// los persistan y actualicen los registros relacionados de remesa y manifiesto
        /// </summary>
        /// <param name="requestDto"></param>
        /// <param name="requestDetail"></param>
        /// <param name="storeProcedure"></param>
        /// <returns></returns>
        public bool ObtenerXmlConsumirWsRndc(RequestDTO requestDto, RequestDetailDTO requestDetail, string storeProcedure)
        {
            //requestDetail debe tener la referencia, el ID, el actionID y el processID
            //limpiar objeto

            var data = new Data();
            var responseTransaction = false;
            requestDetail.Reference = requestDetail.Reference.Trim();
            var hayError = false;

            //linea original de proceso 
            //var values = data.GetInformationAstrans(requestDetail.Reference, storeProcedure, ref thereError);
            //cambio 11/17/2015 para adicionar cor-rem codigo 38 al sp Sp_Rndc_RemesaObtenerXML 


            //Obtiene la data de los SPs de Astrans según el proceso y la formatea con los tags de XML
            var values = data.GetInformationAstrans(requestDetail.Reference, storeProcedure, ref hayError, requestDetail.Processid);
            var templateXml = Resources.Request;


            if (!hayError)
            {
                if (!String.IsNullOrEmpty(values))
                {
                    //se formatea el mensaje final completo con user, password, etc, como debe ir al ministerio
                    templateXml = string.Format(templateXml, User, Password, requestDetail.ActionId, requestDetail.Processid, values);

                    // llamado al WebService del ministerio
                    var serviceResponse = AtenderMensajeRndc(templateXml);

                    //var lista = new List<string>();
                    //lista.Add(templateXml);
                    //lista.Add(serviceResponse);
                    //LogRecordString.WriteLog(lista, string.Format("request_{0}_{1}", requestDto.InterfaceCode, requestDto.InterfaceReference));

                    requestDetail.SoapIn = templateXml;
                    requestDetail.SoapOut = serviceResponse;
                    requestDetail.ProcessDate = DateTime.Now;

                    var response = new Response();  //porqué es una clase?? esto solo tiene un método y solo se usa aquí mismo...
                    var id = false;
                    var reply = response.DecodeReply(serviceResponse, ref id);


                    if (id)
                    {
                        requestDetail.Sucessful = true;
                        requestDetail.TransactionId = reply;
                        responseTransaction = true;
                    }
                    else
                    {
                        requestDetail.Sucessful = false;
                        requestDetail.ErrorDescription = reply + string.Format(Resources.ErrorReference, requestDetail.Reference);
                        requestDto.Sucessful = false;
                        requestDto.ProcessDate = System.DateTime.Now;
                        requestDto.MainErrorDescription = reply + string.Format(Resources.ErrorReference, requestDetail.Reference);
                    }
                }
                else
                {
                    requestDetail.Sucessful = false;
                    requestDetail.ErrorDescription = string.Format(Resources.InformationMessage, storeProcedure, requestDetail.Reference);
                    requestDto.Sucessful = false;
                    requestDto.ProcessDate = System.DateTime.Now.ToLocalTime();
                    requestDto.MainErrorDescription = string.Format(Resources.InformationMessage, storeProcedure, requestDetail.Reference);
                }
            }
            else
            {
                requestDetail.Sucessful = false;
                requestDetail.ErrorDescription = values;
                requestDto.Sucessful = false;
                requestDto.MainErrorDescription = values;
            }
            return responseTransaction;
        }
    }
}
