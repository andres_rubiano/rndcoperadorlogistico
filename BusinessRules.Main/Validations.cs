﻿using System;
using System.Data;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
  public class Validations
  {
    
    public string ValidateEmptyFields(DataSet dataSet)
    {
      var columns = new StringBuilder();
      for (var i = 0; i < dataSet.Tables[0].Columns.Count; i++)
      {
        var column = dataSet.Tables[0].Columns[i].ToString();
        var value = dataSet.Tables[0].Rows[0][column].ToString();
        if (!String.IsNullOrEmpty(value)) continue;
        columns.Append(column + ",");
      }
      return !String.IsNullOrEmpty(columns.ToString()) ? columns.ToString() : string.Empty;
    }

    public string ValidateFields(DataSet dataSet, string reference)
    {
      var interfaceAstrans = Singleton.Instancia.AstransInterface;
      var strErrors = new StringBuilder();
      if (!String.IsNullOrEmpty(ValidateEmptyFields(dataSet)))
        strErrors.Append(string.Format(Resources.ErrorCamposVacios, ValidateEmptyFields(dataSet))); 

      var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
      var cmd = dataBase.GetStoredProcCommand(Resources.Validaciones);
      dataBase.AddInParameter(cmd, "@Interface", DbType.String, interfaceAstrans);
      dataBase.AddInParameter(cmd, "@Reference", DbType.String, reference);
      strErrors.Append(dataBase.ExecuteDataSet(cmd).Tables[0].Rows[0][0].ToString());
      return strErrors.ToString();
    }
  }
}
