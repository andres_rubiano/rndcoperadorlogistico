﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml.Serialization;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;
using Simplexity.AsTrans.RNDC.Infrastructure.Data.Main.EF;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    public class Data
    {
        private LogRecord<List<string>> LogRecordString = new LogRecord<List<string>>(new List<string>());
        private string _xml = TagsXml.Tag;

        //linea original proceso envio inforamacion al sp Sp_Rndc_RemesaObtenerXML
        //public string  GetInformationAstrans(string reference, string storeProcedure, ref bool thereError)
        //Linea cambiada 11/17/2015 para el sp Sp_Rndc_RemesaObtenerXML 
        public string GetInformationAstrans(string reference, string storeProcedure, ref bool thereError, int process)
        {
            var validations = new Validations();
            var formattedXml = new StringBuilder();
            var formattedXmlTripMan = new StringBuilder();
            var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
            string processId = string.Empty;
            var cmd = dataBase.GetStoredProcCommand(storeProcedure);

            //Si se va a registrar sedes se agregan parametros adicionales
            if (storeProcedure == Resources.Sedes)
            {
                var codes = reference.Split('§');
                if (!(codes.Length < 2 || codes.Length >= 3))
                {
                    dataBase.AddInParameter(cmd, "@ReferenceCodeThirdParty", DbType.String, codes[0]);
                    dataBase.AddInParameter(cmd, "@ReferenceCodeLocation", DbType.String, codes[1]);
                }
                else
                    return Resources.ErrorFormat;
            }
            else
            {
                dataBase.AddInParameter(cmd, "@Reference", DbType.String, reference);

                // Linea para agregar el process ID al Sp Sp_Rndc_RemesaObtenerXML
                if (storeProcedure == "Sp_Rndc_RemesaObtenerXML")
                {
                    dataBase.AddInParameter(cmd, "@processId", DbType.Int16, process);
                }
            }
            var dataSet = dataBase.ExecuteDataSet(cmd);

            if (dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                var strError = validations.ValidateFields(dataSet, reference);
                if (String.IsNullOrEmpty(strError))
                {
                    for (int i = 0; i < dataSet.Tables[0].Columns.Count; i++)
                    {
                        var column = dataSet.Tables[0].Columns[i].ToString();
                        var value = dataSet.Tables[0].Rows[0][column].ToString();
                        String[] numRemesas;
                        if (column.Equals("PREREMESAS") && !String.IsNullOrEmpty(value))
                        {
                            numRemesas = value.Split(',');
                            for (int j = 0; j < numRemesas.Length; j++)
                            {
                                formattedXmlTripMan.Append(string.Format(Resources.TagTrip, numRemesas[j]));
                            }
                            processId = TagsXml.ProcessIdRemesa;
                            value = formattedXmlTripMan.ToString();
                        }
                        else if (column.Equals("REMESASMAN") && !String.IsNullOrEmpty(value))
                        {
                            numRemesas = value.Split(',');
                            for (int j = 0; j < numRemesas.Length; j++)
                            {
                                formattedXmlTripMan.Append(string.Format(Resources.TagManifest, numRemesas[j]));
                            }
                            processId = TagsXml.ProcessIdManifiesto;
                            value = formattedXmlTripMan.ToString();
                        }

                        formattedXml.Append(string.Format(_xml, column + processId, value, column));
                        processId = string.Empty;
                    }
                }
                else
                {
                    thereError = true;
                    return strError;
                }
            }
            else
            {
                thereError = true;
                return string.Empty;
            }
            return formattedXml.ToString();
        }

        public void UpdateRequest(RequestDTO requestCandidate)
        {
            using (var rndcEntities = new SPX_RNDC_PRODEntities())
            {
                var request = (from p in rndcEntities.Request
                               where p.RequestId == requestCandidate.RequestId
                               select p).FirstOrDefault();

                if (request != null)
                {
                    request.ProcessDate = DateTime.Now;
                    request.Status = (int)OperationConstants.Status.Processed; //requestCandidate.Status;
                    request.IsSuccessful = requestCandidate.Sucessful;
                    request.TransactionId = requestCandidate.TransactionId;
                    request.MainErrorCode = requestCandidate.MainErrorCode;
                    request.MainErrorDescription = requestCandidate.MainErrorDescription;
                    //request.NextId = requestCandidate.NextId;
                }
                rndcEntities.SaveChanges();
            }
        }

        public void UpdateRequestWithError(RequestDTO requestObj, string message, bool retry)
        {
            using (var rndcEntities = new SPX_RNDC_PRODEntities())
            {
                var request = (from p in rndcEntities.Request
                               where p.RequestId == requestObj.RequestId
                               select p).FirstOrDefault();

                if (request != null)
                {
                    request.ProcessDate = DateTime.Now;
                    request.Status = (int)OperationConstants.Status.Processed;
                    request.IsSuccessful = false;
                    request.MainErrorDescription = message.Truncate(3000);
                }

                if (retry)
                {
                    var newRequest = new Request();

                    newRequest.InterfaceCode = request.InterfaceCode;
                    newRequest.InterfaceReference = request.InterfaceReference;
                    newRequest.RequestDate = DateTime.Now.AddSeconds(10);
                    newRequest.Status = 0;

                    rndcEntities.Request.AddObject(newRequest);
                }

                rndcEntities.SaveChanges();
            }

            if (retry)
            {
                return;
            }

            //Validar que viene:

            switch (requestObj.InterfaceCode)
            {
                case Interfaces.RegisterThirdParty:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.TercerosRegistrarRespuesta);
                    break;

                case Interfaces.RegisterVehicle:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.VehiculosRegistrarRespuesta);
                    break;

                case Interfaces.RegisterLoadingOrder:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.CargaRegistrarRespuesta);
                    break;

                case Interfaces.RegisterrManifest:
                    UpdateResponseErrorIntegrateManifest(string.Empty, requestObj.InterfaceReference,
                        Resources.ErrorIntegracion, Resources.ManifiestoRegistrarRespuesta);
                    break;

                case Interfaces.FulfillManifest:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.CumManifiestoRegistrarRespuesta);
                    break;

                case Interfaces.RegisterLocations:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.SedesRegistrarRespuesta);
                    break;

                case Interfaces.CorrectShipment:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.RemesaRegistrarRespuesta);

                    break;
                case Interfaces.CancelRem:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.AnularRemesaRegistrarRespuesta);
                    break;
                case Interfaces.CancelManifest:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.AnularManifiestoRegistrarRespuesta);
                    break;

                case Interfaces.RegistrarCumplidoInicialRemesa:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.spRemesaCumplidoInicialRegistrarRespuesta);
                    break;

                case Interfaces.AnularCumplidoInicialRemesa:
                    UpdateResponses(string.Empty, requestObj.InterfaceReference, Resources.ErrorIntegracion,
                        IsSucessful.IsNotSucessful, Resources.spAnularCumplidoIniRemesaRegistrarRespuesta);
                    break;

                case Interfaces.ConsultarFirmaElectronicaManifiesto:
                    UpdateResponseQueryProcess(requestObj.InterfaceReference, IsSucessful.IsNotSucessful,
                        Resources.ErrorIntegracion, Resources.spManifiestoRegistrarRespuestaConsultaFirmaElectronica);
                    break;

            }
        }

        public void UpdateResponses(string transactionId, string reference, string errorMessage, string isSucessful, string storedProcedure)
        {
            var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
            var cmd = dataBase.GetStoredProcCommand(storedProcedure);
            dataBase.AddInParameter(cmd, "@Reference", DbType.String, reference);
            dataBase.AddInParameter(cmd, "@Exitoso", DbType.String, isSucessful);
            dataBase.AddInParameter(cmd, "@IngresoId", DbType.String, transactionId);
            dataBase.AddInParameter(cmd, "@MensajeError", DbType.String, errorMessage);
            dataBase.ExecuteDataSet(cmd);
        }

        public void UpdateResponseIntegratedManifest(string transactionId, string reference, string response, string storedProcedure)
        {
            var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
            var cmd = dataBase.GetStoredProcCommand(storedProcedure);
            string urlCodeQr = string.Empty;
            string localPathCodeQr = string.Empty;
            var urlQr = ConfigurationManager.AppSettings["UrlQrImages"];

            // 2018MAy24 para registrar informacion adicional que viene en el response del Rndc, se deja en esta nueva funcion
            //           DAdo que inicialmente no se penso en que a futuro se recibieran mas campos:
            var r = new Response();
            string codigoSeguridadQr;
            string observacion;
            r.DecodeReply(response, out codigoSeguridadQr, out observacion);

            if (!string.IsNullOrEmpty(observacion))
            {
                var textoRemover = ConfigurationManager.AppSettings["ManRemoverTextoEnObservaciones"];
                observacion = observacion.Replace(textoRemover, string.Empty);
            }

            // Generar el codigo QR:
            cmd = dataBase.GetStoredProcCommand(Resources.ManifiestoObtenerInformacionQr);
            dataBase.AddInParameter(cmd, "@manifiestoNumero", DbType.String, reference);

            var manifiestoInformacionQr = dataBase.ExecuteDataSet(cmd);

            if (manifiestoInformacionQr != null && manifiestoInformacionQr.Tables[0].Rows.Count > 0)
            {
                if (string.IsNullOrEmpty(codigoSeguridadQr))
                    codigoSeguridadQr = manifiestoInformacionQr.Tables[0].Rows[0][12].ToString();

                if (string.IsNullOrEmpty(observacion))
                    observacion = manifiestoInformacionQr.Tables[0].Rows[0][11].ToString();

                if (!string.IsNullOrEmpty(observacion))
                    observacion = observacion.TrimEnd('\r', '\n');

                var manifiesto = new ManifestQrInformationDto()
                {
                    ManifiestoNumero = manifiestoInformacionQr.Tables[0].Rows[0][0].ToString(),
                    Mec = transactionId ?? manifiestoInformacionQr.Tables[0].Rows[0][1].ToString(),
                    Fecha = manifiestoInformacionQr.Tables[0].Rows[0][2].ToString(),
                    Placa = manifiestoInformacionQr.Tables[0].Rows[0][3].ToString(),
                    Remolque = manifiestoInformacionQr.Tables[0].Rows[0][4].ToString(),
                    ConfiguracionConjunto = manifiestoInformacionQr.Tables[0].Rows[0][5].ToString(),
                    CiudadOrigen = manifiestoInformacionQr.Tables[0].Rows[0][6].ToString(),
                    CiudadDestino = manifiestoInformacionQr.Tables[0].Rows[0][7].ToString(),
                    Mercancia = manifiestoInformacionQr.Tables[0].Rows[0][8].ToString(),
                    ConductorId = manifiestoInformacionQr.Tables[0].Rows[0][9].ToString(),
                    Empresa = manifiestoInformacionQr.Tables[0].Rows[0][10].ToString(),
                    ObservacionesRndc = observacion,
                    CodigoSeguridadQr = codigoSeguridadQr
                };

                QrCodeGenerator.Create(manifiesto, out urlCodeQr, out localPathCodeQr);
            }

            cmd = dataBase.GetStoredProcCommand(storedProcedure);
            dataBase.AddInParameter(cmd, "@Reference", DbType.String, reference);
            dataBase.AddInParameter(cmd, "@Exitoso", DbType.String, IsSucessful.Sucessful);
            dataBase.AddInParameter(cmd, "@IngresoId", DbType.String, transactionId);
            dataBase.AddInParameter(cmd, "@MensajeError", DbType.String, string.Empty);
            dataBase.AddInParameter(cmd, "@CodigoSeguridadQr", DbType.String, codigoSeguridadQr);
            dataBase.AddInParameter(cmd, "@Observacion", DbType.String, observacion);
            dataBase.AddInParameter(cmd, "@urlCodigoQr", DbType.String, urlQr + urlCodeQr);
            dataBase.AddInParameter(cmd, "@localPathCodigoQr", DbType.String, localPathCodeQr);
            dataBase.ExecuteDataSet(cmd);
        }

        public void UpdateResponseErrorIntegrateManifest(string transactionId, string reference, string errorMessage, string storedProcedure)
        {
            var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
            var cmd = dataBase.GetStoredProcCommand(storedProcedure);
            dataBase.AddInParameter(cmd, "@Reference", DbType.String, reference);
            dataBase.AddInParameter(cmd, "@Exitoso", DbType.String, IsSucessful.IsNotSucessful);
            dataBase.AddInParameter(cmd, "@IngresoId", DbType.String, transactionId);
            dataBase.AddInParameter(cmd, "@MensajeError", DbType.String, errorMessage);
            dataBase.AddInParameter(cmd, "@CodigoSeguridadQr", DbType.String, string.Empty);
            dataBase.AddInParameter(cmd, "@Observacion", DbType.String, string.Empty);
            dataBase.AddInParameter(cmd, "@urlCodigoQr", DbType.String, string.Empty);
            dataBase.AddInParameter(cmd, "@localPathCodigoQr", DbType.String, string.Empty);
            dataBase.ExecuteDataSet(cmd);
        }

        public void UpdateRequestDetail(RequestDetailDTO requestDetailCandidate)
        {
            using (var rndcEntities = new SPX_RNDC_PRODEntities())
            {
                rndcEntities.AddToRequestDetail(new RequestDetail
                {
                    Req_RequestId = requestDetailCandidate.RequestId,
                    ProcessDate = DateTime.Now.ToLocalTime(),
                    ProcessId = requestDetailCandidate.Processid,
                    ActionId = requestDetailCandidate.ActionId,
                    Reference = requestDetailCandidate.Reference,
                    IsSuccessful = requestDetailCandidate.Sucessful,
                    SoapRequest = requestDetailCandidate.SoapIn,
                    SoapResponse = requestDetailCandidate.SoapOut,
                    TransactionId = requestDetailCandidate.TransactionId,
                    ErrorCode = requestDetailCandidate.ErrorCode,
                    ErrorDescription = requestDetailCandidate.ErrorDescription
                });


                rndcEntities.SaveChanges();
            }
        }

        public bool GetParameter(string parameter, string currentVersion)
        {
            var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
            var cmd = dataBase.GetStoredProcCommand(Resources.ObtenerVersion);
            dataBase.AddInParameter(cmd, "@cprCpyCode", DbType.String, parameter);
            var dataSet = dataBase.ExecuteDataSet(cmd);

            if (dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                var column = dataSet.Tables[0].Columns[0].ToString();
                var parameterAstrans = dataSet.Tables[0].Rows[0][column].ToString();

                return parameterAstrans == currentVersion;
            }
            return false;
        }

        public bool GetRunningStatus()
        {
            //var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
            //var cmd = dataBase.GetStoredProcCommand(Resources.ObtenerEstadoDeEjecucion);
            //var dataSet = dataBase.ExecuteDataSet(cmd);

            //if (dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0||String.IsNullOrEmpty(dataSet.Tables[0].Rows[0][0].ToString()))
            //{
            //  var parameterAstrans = dataSet.Tables[0].Rows[0][0].ToString();
            //  return parameterAstrans == "0"||String.IsNullOrEmpty(parameterAstrans);
            //}
            return true;
        }

        public void UpdateResponseQueryProcess(string reference, string isSucessful, string responseQueryXml, string storedProcedure)
        {
            switch (storedProcedure)
            {
                case StoredProcedure.SpRndcManifiestoRegistrarRespuestaFirmaElectronica:

                    // Decodificar el XML:
                    #region old version

                    //string ingresoid = XmlTool.GetValue(responseQueryXml, @"root/documento", @"ingresoId");
                    //string textoFecha = XmlTool.GetValue(responseQueryXml, @"root/documento", @"fechaing");
                    //var textoFechaAjustado = textoFecha.Replace(". ", string.Empty).Replace(".", string.Empty);

                    //DateTime fechaing = Convert.ToDateTime("2000-01-01"); // no interesa este valor, cuando isSucessful = false, no se registra;
                    //if (!string.IsNullOrEmpty(textoFechaAjustado))
                    //    fechaing = Convert.ToDateTime(textoFechaAjustado);

                    //string tipo = XmlTool.GetValue(responseQueryXml, @"root/documento", @"tipo");
                    //string codidconductor = XmlTool.GetValue(responseQueryXml, @"root/documento", @"codidconductor");
                    //string numidconductor = XmlTool.GetValue(responseQueryXml, @"root/documento", @"numidconductor");
                    //string codidtitularmanifiesto = XmlTool.GetValue(responseQueryXml, @"root/documento", @"codidtitularmanifiesto");
                    //string numidtitularmanifiesto = XmlTool.GetValue(responseQueryXml, @"root/documento", @"numidtitularmanifiesto");
                    //string observacion = XmlTool.GetValue(responseQueryXml, @"root/documento", @"observacion");

                    //var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
                    //var cmd = dataBase.GetStoredProcCommand(storedProcedure);
                    //dataBase.AddInParameter(cmd, "@reference", DbType.String, reference);
                    //dataBase.AddInParameter(cmd, "@isSucessful", DbType.String, isSucessful);
                    //dataBase.AddInParameter(cmd, "@ingresoid", DbType.String, ingresoid);
                    //dataBase.AddInParameter(cmd, "@fechaing", DbType.DateTime, fechaing);
                    //dataBase.AddInParameter(cmd, "@tipo", DbType.String, tipo);
                    //dataBase.AddInParameter(cmd, "@codidconductor", DbType.String, codidconductor);
                    //dataBase.AddInParameter(cmd, "@numidconductor", DbType.String, numidconductor);
                    //dataBase.AddInParameter(cmd, "@codidtitularmanifiesto", DbType.String, codidtitularmanifiesto);
                    //dataBase.AddInParameter(cmd, "@numidtitularmanifiesto", DbType.String, numidtitularmanifiesto);
                    //dataBase.AddInParameter(cmd, "@observacion", DbType.String, observacion);
                    //dataBase.ExecuteDataSet(cmd);

                    #endregion

                    XmlSerializer serializer = new XmlSerializer(typeof(RespuestaFirmaElectronica));
                    RespuestaFirmaElectronica result;

                    var lista = new List<string>();
                    lista.Add(responseQueryXml);
                    LogRecordString.WriteLog(lista, string.Format("UpdateResponseQueryProcess_{0}", reference));

                    using (TextReader reader = new StringReader(responseQueryXml))
                    {
                        result = (RespuestaFirmaElectronica)serializer.Deserialize(reader);
                    }

                    string conductorIngresoid = string.Empty;
                    DateTime conductorFechaIngreso = new DateTime(2000, 1, 1);
                    string conductorCodidconductor = string.Empty;
                    string conductorNumidconductor = string.Empty;
                    string conductorObservacion = string.Empty;

                    string titularIngresoid = string.Empty;
                    DateTime titularFechaIngreso = new DateTime(2000, 1, 1);
                    string titularCodidtitularmanifiesto = string.Empty;
                    string titularNumidtitularmanifiesto = string.Empty;
                    string titularObservacion = string.Empty;

                    bool existeUnaFirma = false;

                    foreach (var res in result.Documentos)
                    {
                        if (res.Tipo.Equals("C"))
                        {
                            // Hay firma de conductor:
                            conductorIngresoid = res.Ingresoid;
                            conductorFechaIngreso =
                                Convert.ToDateTime(res.Fechaing.Replace(". ", string.Empty).Replace(".", string.Empty));
                            conductorCodidconductor = res.Codidconductor;
                            conductorNumidconductor = res.Numidconductor;
                            conductorObservacion = res.Observacion;

                            existeUnaFirma = true;
                        }

                        if (res.Tipo.Equals("T"))
                        {
                            // Hay firma de titular:
                            titularIngresoid = res.Ingresoid;
                            titularFechaIngreso =
                                Convert.ToDateTime(res.Fechaing.Replace(". ", string.Empty).Replace(".", string.Empty));
                            titularCodidtitularmanifiesto = res.Codidtitularmanifiesto;
                            titularNumidtitularmanifiesto = res.Numidtitularmanifiesto;
                            titularObservacion = res.Observacion;

                            existeUnaFirma = true;
                        }
                    }

                    if (existeUnaFirma)
                    {

                        var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
                        var cmd = dataBase.GetStoredProcCommand(storedProcedure);

                        dataBase.AddInParameter(cmd, "@reference", DbType.String, reference);
                        dataBase.AddInParameter(cmd, "@isSucessful", DbType.String, isSucessful);

                        if (string.IsNullOrEmpty(conductorIngresoid))
                        {
                            dataBase.AddInParameter(cmd, "@ingresoid", DbType.String, null);
                            dataBase.AddInParameter(cmd, "@fechaingConductor", DbType.DateTime, null);
                            dataBase.AddInParameter(cmd, "@codidconductor", DbType.String, null);
                            dataBase.AddInParameter(cmd, "@numidconductor", DbType.String, null);
                            conductorObservacion = string.Empty;
                        }
                        else
                        {
                            dataBase.AddInParameter(cmd, "@ingresoid", DbType.String, conductorIngresoid);
                            dataBase.AddInParameter(cmd, "@fechaingConductor", DbType.DateTime, conductorFechaIngreso);
                            dataBase.AddInParameter(cmd, "@codidconductor", DbType.String, conductorCodidconductor);
                            dataBase.AddInParameter(cmd, "@numidconductor", DbType.String, conductorNumidconductor);
                        }

                        if (string.IsNullOrEmpty(titularIngresoid))
                        {
                            dataBase.AddInParameter(cmd, "@ingresoidTitular", DbType.String, null);
                            dataBase.AddInParameter(cmd, "@fechaingTitular", DbType.DateTime, null);
                            dataBase.AddInParameter(cmd, "@codidtitularmanifiesto", DbType.String, null);
                            dataBase.AddInParameter(cmd, "@numidtitularmanifiesto", DbType.String, null);
                            titularObservacion = string.Empty;
                        }
                        else
                        {
                            dataBase.AddInParameter(cmd, "@ingresoidTitular", DbType.String, titularIngresoid);
                            dataBase.AddInParameter(cmd, "@fechaingTitular", DbType.DateTime, titularFechaIngreso);
                            dataBase.AddInParameter(cmd, "@codidtitularmanifiesto", DbType.String, titularCodidtitularmanifiesto);
                            dataBase.AddInParameter(cmd, "@numidtitularmanifiesto", DbType.String, titularNumidtitularmanifiesto);
                        }

                        var observacion = string.Empty;

                        if (!string.IsNullOrEmpty(conductorObservacion))
                            observacion = string.Format("Conductor: [{0}]", conductorObservacion);

                        if (!string.IsNullOrEmpty(observacion))
                            observacion = observacion + ", ";

                        if (!string.IsNullOrEmpty(titularObservacion))
                            observacion = string.Format("{0}Titular: [{1}]", observacion, titularObservacion);

                        dataBase.AddInParameter(cmd, "@observacion", DbType.String, observacion);

                        dataBase.ExecuteDataSet(cmd);
                    }
                    break;
            }


        }
    }
}