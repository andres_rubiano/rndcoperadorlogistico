﻿using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    public static class QrCodeGenerator
    {
        public static void Create(ManifestQrInformationDto manifestInformation, out string urlQr, out string localPathUrl)
        {
            // convierte la informacion en el formato:
            var qrInfo = ConvertToQrInformation(manifestInformation);

            // carpeta de imagenes QR:
            var pathQr = ConfigurationManager.AppSettings["PathQrImages"];

            // Control de estructura de carpetas: YYYY\MM\DD:

            var pathStruct = manifestInformation.Fecha.Substring(0, 4) + @"\" + manifestInformation.Fecha.Substring(5, 2) + @"\" + manifestInformation.Fecha.Substring(8, 2) + @"\";
            pathQr += pathStruct;
            if (!Directory.Exists(pathQr))
                Directory.CreateDirectory(pathQr);

            // Generar el QR:

            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = qrEncoder.Encode(qrInfo);

            GraphicsRenderer renderer = new GraphicsRenderer(new FixedModuleSize(5, QuietZoneModules.Two), Brushes.Black, Brushes.White);
            using (FileStream stream = new FileStream(pathQr + manifestInformation.ManifiestoNumero + ".png", FileMode.Create))
            {
                renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, stream);
            }

            urlQr = (pathStruct + manifestInformation.ManifiestoNumero + ".png").Replace(@"\", "/");
            localPathUrl = pathQr + manifestInformation.ManifiestoNumero + ".png";

        }

        private static string ConvertToQrInformation(ManifestQrInformationDto manifest)
        {

            string qrInformation = string.Format("MEC:{0}\r\n", manifest.Mec);
            qrInformation += string.Format("Fecha:{0}\r\n", manifest.Fecha);
            qrInformation += string.Format("Placa:{0}\r\n", manifest.Placa);

            if (!string.IsNullOrEmpty(manifest.Remolque))
                qrInformation += string.Format("Remolque:{0}\r\n", manifest.Remolque);

            qrInformation += string.Format("Config:{0}\r\n", manifest.ConfiguracionConjunto);
            qrInformation += string.Format("Orig:{0}\r\n", manifest.CiudadOrigen);
            qrInformation += string.Format("Dest:{0}\r\n", manifest.CiudadDestino);
            qrInformation += string.Format("Mercancia:{0}\r\n", manifest.Mercancia);
            qrInformation += string.Format("Conductor:{0}\r\n", manifest.ConductorId);
            qrInformation += string.Format("Empresa:{0}\r\n", manifest.Empresa);

            if (!string.IsNullOrEmpty(manifest.ObservacionesRndc))
                qrInformation += string.Format("Obs:{0}\r\n", manifest.ObservacionesRndc);

            qrInformation += string.Format("Seguro:{0}", manifest.CodigoSeguridadQr);

            return qrInformation;
        }

    }
}