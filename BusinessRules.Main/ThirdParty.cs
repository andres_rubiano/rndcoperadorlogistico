﻿using System;
using System.Collections.Generic;
using Common;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.co.gov.mintransporte.rndc;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
  public class ThirdParty
  {
    public void ProcessRegisterThirdParty(RequestDTO requestDTO)
    {
      var transactions = new Transactions();
      var data = new Data();
      var processId = (int)RequestType.ProcessId.CrearOActualizarDatosDeTercero;
      var actionId = (int)RequestType.ActionId.Registrar;

      var requestDetail = new RequestDetailDTO
                            {
                              RequestId = requestDTO.RequestId,
                              ActionId = actionId,
                              Processid = processId,
                              Reference = requestDTO.InterfaceReference
                            };

      if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.Terceros))
      {
        //InterfaceSuccesful = false;
        data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription,
                             IsSucessful.IsNotSucessful, Resources.TercerosRegistrarRespuesta);
        data.UpdateRequestDetail(requestDetail);
      }
      else
      {
        data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.Sucessful, Resources.TercerosRegistrarRespuesta);
        data.UpdateRequestDetail(requestDetail);
        requestDTO.Sucessful = true;
        requestDTO.TransactionId = requestDetail.TransactionId;
      }
      data.UpdateRequest(requestDTO);

    }
  }
}


