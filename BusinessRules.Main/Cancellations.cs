﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    public class Cancellations
    {
        private Transactions transactions = new Transactions();

        //public void CancelLoad(RequestDTO requestDTO)
        //{
        //  bool InterfaceSuccesful = true;
        //  var data = new Data();
        //  string serviceResponse;
        //  var processId = (int)RequestType.ProcessId.AnularInformacionDeCarga;
        //  var actionId = (int)RequestType.ActionId.Registrar;

        //  var requestDetail = new RequestDetailDTO();

        //  requestDetail.RequestId = requestDTO.RequestId;
        //  requestDetail.ActionId = actionId;
        //  requestDetail.Processid = processId;
        //  requestDetail.Reference = requestDTO.InterfaceReference;

        //  if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.AnularInfoCarga))
        //  {
        //    //InterfaceSuccesful = false;
        //    data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription,
        //                         IsSucessful.IsNotSucessful, Resources.AnularInfoCargaRegistrarRespuesta);
        //    data.UpdateRequestDetail(requestDetail);
        //  }
        //  else
        //  {
        //    data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.Sucessful, Resources.AnularInfoCargaRegistrarRespuesta);
        //    data.UpdateRequestDetail(requestDetail);
        //  }
        //  data.UpdateRequest(requestDTO);
        //}

        //public void CancelTrip(RequestDTO requestDTO)
        //{
        //  bool InterfaceSuccesful = true;
        //  var data = new Data();
        //  string serviceResponse;
        //  var processId = (int)RequestType.ProcessId.AnularInformacionDelViaje;
        //  var actionId = (int)RequestType.ActionId.Registrar;

        //  var requestDetail = new RequestDetailDTO();

        //  requestDetail.RequestId = requestDTO.RequestId;
        //  requestDetail.ActionId = actionId;
        //  requestDetail.Processid = processId;
        //  requestDetail.Reference = requestDTO.InterfaceReference;

        //  if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.AnularInfoViaje))
        //  {
        //    //InterfaceSuccesful = false;
        //    data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription,
        //                         IsSucessful.IsNotSucessful, Resources.AnularInfoViajeRegistrarRespuesta);
        //    data.UpdateRequestDetail(requestDetail);
        //  }
        //  else
        //  {
        //    data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.Sucessful, Resources.AnularInfoViajeRegistrarRespuesta);
        //    data.UpdateRequestDetail(requestDetail);
        //  }
        //  data.UpdateRequest(requestDTO);
        //}

        public void CancelRem(RequestDTO requestDTO)
        {
            var data = new Data();
            var processId = (int)RequestType.ProcessId.AnularRemesaTerrestreDeCarga;
            var actionId = (int)RequestType.ActionId.Registrar;

            var requestDetail = new RequestDetailDTO
                                  {
                                      RequestId = requestDTO.RequestId,
                                      ActionId = actionId,
                                      Processid = processId,
                                      Reference = requestDTO.InterfaceReference
                                  };

            if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.AnularRemesa))
            {
                //InterfaceSuccesful = false;
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference,
                    requestDetail.ErrorDescription, IsSucessful.IsNotSucessful, Resources.AnularRemesaRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
            }
            else
            {
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference,
                    requestDetail.ErrorDescription, IsSucessful.Sucessful, Resources.AnularRemesaRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
                requestDTO.SetSucessful(requestDetail.TransactionId);
            }
            data.UpdateRequest(requestDTO);
        }

        public void CancelManifest(RequestDTO requestDTO)
        {
            var data = new Data();
            var processId = (int)RequestType.ProcessId.AnularManifiestoDeCarga;
            var actionId = (int)RequestType.ActionId.Registrar;

            var requestDetail = new RequestDetailDTO
                                  {
                                      RequestId = requestDTO.RequestId,
                                      ActionId = actionId,
                                      Processid = processId,
                                      Reference = requestDTO.InterfaceReference
                                  };


            if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.AnularManifiesto))
            {
                //InterfaceSuccesful = false;
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference,
                    requestDetail.ErrorDescription, IsSucessful.IsNotSucessful,
                    Resources.AnularManifiestoRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
            }
            else
            {
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference,
                    requestDetail.ErrorDescription, IsSucessful.Sucessful, Resources.AnularManifiestoRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
                requestDTO.SetSucessful(requestDetail.TransactionId);
            }
            data.UpdateRequest(requestDTO);
        }

        /// <summary>
        /// Cancela el cumplido inicial de una remesa
        /// </summary>
        /// <param name="requestDTO"></param>
        public void CancelarCumplidoInicialRemesa(RequestDTO requestDTO)
        {
            var data = new Data();
            var processId = (int)RequestType.ProcessId.AnularCumplidoInicialRemesa;
            var actionId = (int)RequestType.ActionId.Registrar;

            var requestDetail = new RequestDetailDTO
            {
                RequestId = requestDTO.RequestId,
                ActionId = actionId,
                Processid = processId,
                Reference = requestDTO.InterfaceReference
            };


            if (!transactions.ObtenerXmlConsumirWsRndc(requestDTO, requestDetail, Resources.spAnularCumplidoIniRemesaObtenerXML))
            {
                //InterfaceSuccesful = false;
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference,
                    requestDetail.ErrorDescription, IsSucessful.IsNotSucessful,
                    Resources.spAnularCumplidoIniRemesaRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
            }
            else
            {
                data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference,
                    requestDetail.ErrorDescription, IsSucessful.Sucessful,
                    Resources.spAnularCumplidoIniRemesaRegistrarRespuesta);
                data.UpdateRequestDetail(requestDetail);
                requestDTO.SetSucessful(requestDetail.TransactionId);
            }
            data.UpdateRequest(requestDTO);
        }
    }
}
