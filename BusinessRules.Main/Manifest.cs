﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Threading;
using System.Xml;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.co.gov.mintransporte.rndc;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    public class Manifest
    {
        private LogRecord<List<string>> LogRecordString = new LogRecord<List<string>>(new List<string>());

        public void ProcessRegisterManifest(RequestDTO requestDTO)
        {
            var data = new Data();
            var transactions = new Transactions();
            var interfaceSuccesful = true;
            var processId = (int)RequestType.ProcessId.ExpedirRemesaTerrestreDeCarga;
            var actionId = (int)RequestType.ActionId.Registrar;
            var requestDetail = new RequestDetailDTO();
            string TemplateXml = Resources.TemplateQueryXml;

            var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
            var cmd = dataBase.GetStoredProcCommand(Resources.ManifiestoObtenerRemesas);
            dataBase.AddInParameter(cmd, "@Reference", DbType.String, requestDTO.InterfaceReference);

            var remesasManifiesto = dataBase.ExecuteDataSet(cmd);

            //creamos un objeto de detalle
            requestDetail.RequestId = requestDTO.RequestId;
            requestDetail.ActionId = actionId;
            requestDetail.Processid = processId;

            if (remesasManifiesto.Tables.Count <= 0 || remesasManifiesto.Tables[0].Rows.Count <= 0)
            {
                requestDetail.ErrorDescription =
                    requestDTO.MainErrorDescription = string.Format(Resources.InformationMessage, Resources.ManifiestoObtenerRemesas, requestDTO.InterfaceReference);
                data.UpdateRequestDetail(requestDetail);
                data.UpdateRequest(requestDTO);
                data.UpdateResponseErrorIntegrateManifest(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.ErrorDescription, Resources.ManifiestoRegistrarRespuesta);
                //gestion del error
                //requestDTO.ReportERROR
                return;
            }

            //Reportamos todas las cargas una a una
            if (remesasManifiesto.Tables.Count > 0 && remesasManifiesto.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < remesasManifiesto.Tables[0].Rows.Count; i++)
                {
                    var value = remesasManifiesto.Tables[0].Rows[i][1].ToString();
                    if (!String.IsNullOrEmpty(value)) continue;

                    int timeOut;
                    int.TryParse(ConfigurationManager.AppSettings["WAITBETWEENREQUETS"], out timeOut);
                    Thread.Sleep(timeOut);
                    requestDetail.Reference = remesasManifiesto.Tables[0].Rows[i][0].ToString();
                    if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.Remesa))
                    {
                        interfaceSuccesful = false;
                        //aqui para que se sobrepise esta variable en el LOOP
                        data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.IsNotSucessful, Resources.RemesaRegistrarRespuesta);
                        data.UpdateRequestDetail(requestDetail);
                        //afectar el pedido ASTRANS con el error (SP)
                    }
                    else
                    {
                        data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, null, IsSucessful.Sucessful, Resources.RemesaRegistrarRespuesta);
                        data.UpdateRequestDetail(requestDetail);
                        //afectar el pedido ASTRANS con el idTransaccion, limpiamos Error Description (respons del SP)
                    }
                }

                //Registrar manifiesto
                if (interfaceSuccesful)
                {
                    requestDetail.Reference = requestDTO.InterfaceReference;
                    requestDetail.ActionId = (int)RequestType.ActionId.Registrar;
                    requestDetail.Processid = (int)RequestType.ProcessId.ExpedirManifiestoDeCarga;

                    if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.Manifiesto))
                    {
                        data.UpdateResponseErrorIntegrateManifest(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.ErrorDescription, Resources.ManifiestoRegistrarRespuesta);
                        data.UpdateRequestDetail(requestDetail);
                        //afectar el VIAJE ASTRANS con las malas noticias
                    }
                    else
                    {
                        // MANIFIESTO INTEGRADO CORRECTAMENTE:
                        // data.UpdateResponses(requestDetail.TransactionId, requestDTO.InterfaceReference, null, IsSucessful.Sucessful, Resources.ManifiestoRegistrarRespuesta);

                        data.UpdateResponseIntegratedManifest(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.SoapOut, Resources.ManifiestoRegistrarRespuesta);
                        data.UpdateRequestDetail(requestDetail);
                        requestDTO.Sucessful = true;
                        requestDTO.TransactionId = requestDetail.TransactionId;
                        //afectar el VIAJE ASTRANS con el idTransaccion, limpiamos Error Description (respons del SP)
                    }
                }
                else
                {
                    data.UpdateResponseErrorIntegrateManifest(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.ErrorDescription, Resources.ManifiestoRegistrarRespuesta);
                    //afectar el VIAJE ASTRANS con las malas noticias, error reportando pedido
                }

                data.UpdateRequest(requestDTO);

            }
        }

        public void GetElectronicSignature(RequestDTO requestDto)
        {
            // Obtener el xml de consulta 
            var data = new Data();
            string isSucessfull = string.Empty;
            var requestDetail = new RequestDetailDTO();

            //creamos un objeto de detalle
            requestDetail.RequestId = requestDto.RequestId;
            requestDetail.ActionId = (int)RequestType.ActionId.Consultar;
            requestDetail.Processid = (int)RequestType.ProcessId.ConsultarFirmaElectronica;
            requestDetail.Reference = requestDto.InterfaceReference;

            // Obetener inforamcion del TMS
            var thereError = false;
            var valuesInXmlFormat = data.GetInformationAstrans(requestDetail.Reference, Resources.spConsultarFirmaElectronicaManifiesto, ref thereError, requestDetail.Processid);

            if (thereError)
            {
                requestDetail.Sucessful = false;
                requestDetail.ErrorDescription = string.Format(Resources.InformationMessage,
                    Resources.spConsultarFirmaElectronicaManifiesto, requestDetail.Reference);
                requestDto.Sucessful = false;
                requestDto.ProcessDate = DateTime.Now.ToLocalTime();
                requestDto.MainErrorDescription = string.Format(Resources.InformationMessage,
                    Resources.spConsultarFirmaElectronicaManifiesto, requestDetail.Reference);

                data.UpdateRequest(requestDto);
                data.UpdateRequestDetail(requestDetail);
            }
            else
            {
                if (string.IsNullOrEmpty(valuesInXmlFormat))
                {
                    requestDetail.Sucessful = false;
                    requestDetail.ErrorDescription = string.Format(Resources.InformationMessage,
                        Resources.spConsultarFirmaElectronicaManifiesto, requestDetail.Reference);
                    requestDto.Sucessful = false;
                    requestDto.ProcessDate = DateTime.Now.ToLocalTime();
                    requestDto.MainErrorDescription = string.Format(Resources.InformationMessage,
                        Resources.spConsultarFirmaElectronicaManifiesto, requestDetail.Reference);
                    data.UpdateRequest(requestDto);
                    data.UpdateRequestDetail(requestDetail);
                }
                else
                {
                    // Solicitar la info a Rndc:
                    var objectQueryRequest = new QueryRequest();
                    var listQuery = objectQueryRequest.GetListOfClassQuery(valuesInXmlFormat);

                    string requestXml = string.Empty;
                    var respuesta = objectQueryRequest.GetInformation(listQuery, requestDetail.Processid, out requestXml);

                    //var lista = new List<string>();
                    //lista.Add(requestXml);
                    //lista.Add(respuesta);
                    //LogRecordString.WriteLog(lista, string.Format("request_{0}_{1}", requestDto.InterfaceCode, requestDto.InterfaceReference));
                    
                    var response = new Response();
                    var id = false;
                    var reply = response.DecodeReply(respuesta, ref id);

                    if (id)
                    {
                        isSucessfull = IsSucessful.Sucessful;

                        requestDto.Sucessful = true;
                        requestDto.TransactionId = reply;

                        requestDetail.Sucessful = true;
                        requestDetail.TransactionId = reply;
                    }
                    else
                    {
                        isSucessfull = IsSucessful.IsNotSucessful;

                        requestDto.Sucessful = false;

                        requestDetail.Sucessful = false;
                        requestDetail.ErrorDescription = reply + string.Format(Resources.ErrorReference, requestDetail.Reference);

                        requestDto.MainErrorDescription = reply + string.Format(Resources.ErrorReference, requestDetail.Reference);
                    }

                    requestDto.ProcessDate = DateTime.Now;

                    requestDetail.SoapIn = requestXml;
                    requestDetail.SoapOut = respuesta;

                    data.UpdateResponseQueryProcess(requestDto.InterfaceReference, isSucessfull, requestDetail.SoapOut,
                        Resources.spManifiestoRegistrarRespuestaConsultaFirmaElectronica);
                    data.UpdateRequest(requestDto);
                    data.UpdateRequestDetail(requestDetail);
                }
            }



        }
    }
}