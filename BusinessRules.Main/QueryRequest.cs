﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Common;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.co.gov.mintransporte.rndc;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    public class QueryRequest : Base
    {
        public string GetInformation(List<ClassQuery> listClassQuery, int processId, out string formattedXml)
        {
            var formattedXMLVariables = new StringBuilder();
            var formattedXMLDocumento = new StringBuilder();
            string TemplateXml = Resources.TemplateQueryXml;
            string response;

            foreach (var classQuery in listClassQuery)
            {
                if (classQuery.fielQuery)
                    formattedXMLVariables.Append(classQuery.field + TagsXml.Split);

                if (classQuery.fieldCondition != null)
                    formattedXMLDocumento.Append(string.Format(TagsXml.TagQuery, classQuery.field, classQuery.fieldCondition,
                                                               classQuery.field));
            }

            if (formattedXMLVariables.Length > 1)
                formattedXMLVariables.Replace(',', ' ', formattedXMLVariables.Length - 1, 1);

            formattedXml = string.Format(TemplateXml, User, Password, (int)RequestType.ActionId.Consultar,
                                         processId, formattedXMLVariables, formattedXMLDocumento);

            response = AtenderMensajeRndc(formattedXml);
            
            return response;

        }

        public List<ClassQuery> GetListOfClassQuery(string valuesInXmlFormat)
        {
            var lista = new List<ClassQuery>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(string.Format("<DATA>{0}</DATA>", valuesInXmlFormat));

            var nodes = xmlDoc.SelectNodes(@"/DATA");

            foreach (XmlNode data in nodes)
            {
                foreach (XmlNode tagXml in data)
                {
                    var datos = tagXml.InnerText.Split('|');
                    var query = new ClassQuery();
                    query.field = tagXml.Name;
                    query.fielQuery = Convert.ToBoolean(datos[1]);
                    query.fieldCondition = string.IsNullOrEmpty(datos[0]) ? null : datos[0];

                    lista.Add(query);
                }
            }

            return lista;
        }
    }
}
