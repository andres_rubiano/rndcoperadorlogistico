﻿using Common;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    public class Response
    {
        public string DecodeReply(string xml, ref bool id)
        {
            var reply = "";
            var indexStart = 0;
            var indexEnd = 0;

            indexStart = xml.IndexOf(TagsXml.TagStartId);
            if (indexStart != -1)
            {
                indexEnd = xml.IndexOf(TagsXml.TagEndId);
                reply = xml.Substring(indexStart + TagsXml.TagStartId.Length,
                  indexEnd - (indexStart + TagsXml.TagStartId.Length));
                id = true;
            }
            else
            {
                indexStart = xml.IndexOf(TagsXml.TagStartError);
                if (indexStart != -1)
                {
                    indexEnd = xml.IndexOf(TagsXml.TagEndError);
                    reply = xml.Substring(indexStart + TagsXml.TagStartError.Length,
                      indexEnd - (indexStart + TagsXml.TagStartError.Length));

                    //Este bloque de codigo verifica si las remesas o manifiestos ya fueron reportados y de ser asi devuelve un id de transaccion
                    var thereDuplicate = xml.IndexOf(TagsXml.DuplicateStart);

                    if (thereDuplicate != -1)
                    {
                        //var elementsDuplicate = new[] { TagsXml.IsRemDuplicate, TagsXml.IsManDuplicate, TagsXml.IsCumRemDuplicate, TagsXml.IsCumManDuplicate };

                        //foreach (var t in elementsDuplicate)
                        //{
                        //  var elementDuplicate = xml.IndexOf(t);
                        //  if (elementDuplicate == -1) continue;
                        //indexStart = xml.IndexOf(TagsXml.DuplicateStart);
                        //  switch (t)
                        //  {
                        //    case TagsXml.IsRemDuplicate:
                        //      indexEnd = xml.IndexOf(TagsXml.RemDuplicateEnd);
                        //      break;
                        //    case TagsXml.IsManDuplicate:
                        //      indexEnd = xml.IndexOf(TagsXml.ManDuplicateEnd);
                        //      break;
                        //    case TagsXml.IsCumRemDuplicate:
                        //      indexEnd = xml.IndexOf(TagsXml.CumRemDuplicateEnd);
                        //      break;
                        //    case TagsXml.IsCumManDuplicate:
                        //      indexEnd = xml.IndexOf(TagsXml.CumManDuplicateEnd);
                        //      break;
                        //  }

                        // Reply tiene la cadena desde la palabra "DUPICADO:"

                        indexStart = reply.IndexOf(TagsXml.DuplicateIdStart);
                        indexEnd = reply.IndexOf(TagsXml.DuplicateIdEnd);
                        reply = reply.Substring(indexStart + 1, indexEnd - indexStart);
                        reply = reply.Trim();
                        id = true;
                        //}
                    }
                }
            }
            return reply;
        }

        public void DecodeReply(string xml, out string securityCode, out string observation)
        {
            var indexStart = 0;
            var indexEnd = 0;
            securityCode = string.Empty;
            observation = string.Empty;

            indexStart = xml.IndexOf(TagsXml.TagStartSeguridadQr, System.StringComparison.Ordinal);
            if (indexStart != -1)
            {
                indexEnd = xml.IndexOf(TagsXml.TagEndSeguridadQr, System.StringComparison.Ordinal);
                securityCode = xml.Substring(indexStart + TagsXml.TagStartSeguridadQr.Length, indexEnd - (indexStart + TagsXml.TagStartSeguridadQr.Length));
            }

            indexStart = xml.IndexOf(TagsXml.TagStartObservaciones, System.StringComparison.Ordinal);
            if (indexStart != -1)
            {
                indexEnd = xml.IndexOf(TagsXml.TagEndObservaciones, System.StringComparison.Ordinal);
                observation = xml.Substring(indexStart + TagsXml.TagStartObservaciones.Length, indexEnd - (indexStart + TagsXml.TagStartObservaciones.Length));
            }
        }
    }
}