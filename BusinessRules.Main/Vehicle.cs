﻿using Common;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;


namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
  public class Vehicle
  {
    public void ProcessRegisterVehicle(RequestDTO requestDTO)
    {
      var transactions = new Transactions();
      var data = new Data();
      var processId = (int)RequestType.ProcessId.CrearOActualizarDatosDeVehiculo;
      var actionId = (int)RequestType.ActionId.Registrar;

      var requestDetail = new RequestDetailDTO
                            {
                              RequestId = requestDTO.RequestId,
                              ActionId = actionId,
                              Processid = processId,
                              Reference = requestDTO.InterfaceReference
                            };

      if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.Vehiculos))
      {
        data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.IsNotSucessful, Resources.VehiculosRegistrarRespuesta);
        data.UpdateRequestDetail(requestDetail);
      }
      else
      {
        data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.Sucessful, Resources.VehiculosRegistrarRespuesta);
        data.UpdateRequestDetail(requestDetail);
        requestDTO.Sucessful = true;
        requestDTO.TransactionId = requestDetail.TransactionId;
        //afectar vehiculos
      }
      data.UpdateRequest(requestDTO);
    }
  }
}
