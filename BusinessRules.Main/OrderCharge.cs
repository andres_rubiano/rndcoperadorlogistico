﻿using System.Data;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
  public class OrderCharge
  {
    public void ReportLoadingOrderProcess(RequestDTO requestDTO)
    {
      var data = new Data();
      var transactions = new Transactions();
      bool InterfaceSuccesful = true;
      var processId = (int)RequestType.ProcessId.RegistrarInformacionDeCarga;
      var actionId = (int)RequestType.ActionId.Registrar;
      var requestDetail = new RequestDetailDTO();

      var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
      var cmd = dataBase.GetStoredProcCommand(Resources.InformacionDespacho);
      dataBase.AddInParameter(cmd, "@Reference", DbType.String, requestDTO.InterfaceReference);

      var dataSet = dataBase.ExecuteDataSet(cmd);

      //creamos un objeto de detalle
      requestDetail.RequestId = requestDTO.RequestId;
      requestDetail.ActionId = actionId;
      requestDetail.Processid = processId;

      if (dataSet.Tables.Count <= 0 || dataSet.Tables[0].Rows.Count <= 0)
      {
        requestDetail.ErrorDescription = requestDTO.MainErrorDescription = string.Format(Resources.InformationMessage, Resources.InformacionDespacho, requestDTO.InterfaceReference);
        data.UpdateRequestDetail(requestDetail);
        data.UpdateRequest(requestDTO);
        data.UpdateResponses(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.ErrorDescription, IsSucessful.IsNotSucessful, Resources.ViajeRegistrarRespuesta);
        //gestion del error
        //requestDTO.ReportERROR
        return;
      }
  
      //Reportamos todas las cargas una a una
      if (dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
      {
        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        {
          requestDetail.Reference = dataSet.Tables[0].Rows[i][0].ToString();
          if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.InformacionCarga))
          {
            InterfaceSuccesful = false;
            //aqui para que se sobrepise esta variable en el LOOP
            data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.IsNotSucessful, Resources.CargaRegistrarRespuesta);
            data.UpdateRequestDetail(requestDetail);
            //afectar el pedido ASTRANS con el error (SP)
          }
          else
          {
            data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, null, IsSucessful.Sucessful, Resources.CargaRegistrarRespuesta);
            data.UpdateRequestDetail(requestDetail);
            //afectar el pedido ASTRANS con el idTransaccion, limpiamos Error Description (respons del SP)
          }
        }

        if (InterfaceSuccesful)
        {
          requestDetail.Reference = requestDTO.InterfaceReference;
          requestDetail.ActionId = (int)RequestType.ActionId.Registrar;
          requestDetail.Processid = (int)RequestType.ProcessId.RegistrarInformacionDeViaje;

          if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.InformacionViaje))
          {
            data.UpdateResponses(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.ErrorDescription, IsSucessful.IsNotSucessful, Resources.ViajeRegistrarRespuesta);
            data.UpdateRequestDetail(requestDetail);
            //afectar el VIAJE ASTRANS con las malas noticias
          }
          else
          {
            data.UpdateResponses(requestDetail.TransactionId, requestDTO.InterfaceReference, null, IsSucessful.Sucessful, Resources.ViajeRegistrarRespuesta);
            data.UpdateRequestDetail(requestDetail);
            requestDTO.Sucessful = true;
            requestDTO.TransactionId = requestDetail.TransactionId;
            //afectar el VIAJE ASTRANS con el idTransaccion, limpiamos Error Description (respons del SP)
          }
          
        }
        else
        {
          data.UpdateResponses(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.ErrorDescription, IsSucessful.IsNotSucessful, Resources.ViajeRegistrarRespuesta);
          //data.UpdateRequestDetail(requestDetail);
          //afectar el VIAJE ASTRANS con las malas noticias, error reportando pedido
        }

        //requestDTO.Details = new List<RequestDetailDTO>();
        //requestDTO.Details.Add(requestDetail);
        //data.UpdateRequestDetail(requestDetail);
        data.UpdateRequest(requestDTO);

      }
    }
  }
}
