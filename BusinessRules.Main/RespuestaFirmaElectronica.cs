﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
    [XmlRoot(ElementName = "root")]
    public class RespuestaFirmaElectronica
    {
        public RespuestaFirmaElectronica()
        {
            Documentos = new List<Documento>();
        }
        [XmlElement("documento")]
        public List<Documento> Documentos { get; set; }
    }

    public class Documento
    {
        [XmlElement("ingresoid")]
        public string Ingresoid { get; set; }

        [XmlElement("fechaing")]
        public string Fechaing { get; set; }

        [XmlElement("tipo")]
        public string Tipo { get; set; }

        [XmlElement("codidconductor")]
        public string Codidconductor { get; set; }

        [XmlElement("numidconductor")]
        public string Numidconductor { get; set; }

        [XmlElement("codidtitularmanifiesto")]
        public string Codidtitularmanifiesto { get; set; }

        [XmlElement("numidtitularmanifiesto")]
        public string Numidtitularmanifiesto { get; set; }

        [XmlElement("observacion")]
        public string Observacion { get; set; }
    }
}
