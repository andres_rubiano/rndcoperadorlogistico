﻿using System;
using System.Configuration;
using System.Data;
using System.Threading;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
  public class FulfillManifest
  {
    public void ProcessFulfillManifest(RequestDTO requestDTO)
    {
      var data = new Data();
      var transactions = new Transactions();

      var interfaceSuccesful = true;
      var processId = (int) RequestType.ProcessId.CumplirRemesaTerrestreDeCarga;
      var actionId = (int) RequestType.ActionId.Registrar;
      var requestDetail = new RequestDetailDTO();

      var dataBase = DatabaseFactory.CreateDatabase("ClientDB");
      var cmd = dataBase.GetStoredProcCommand(Resources.CumManifiestoObtenerRemesas);
      dataBase.AddInParameter(cmd, "@Reference", DbType.String, requestDTO.InterfaceReference);

      var dataSet = dataBase.ExecuteDataSet(cmd);

      //creamos un objeto de detalle
      requestDetail.RequestId = requestDTO.RequestId;
      requestDetail.ActionId = actionId;
      requestDetail.Processid = processId;

      if (dataSet.Tables.Count <= 0 || dataSet.Tables[0].Rows.Count <= 0)
      {
        requestDetail.ErrorDescription = requestDTO.MainErrorDescription = string.Format(Resources.InformationMessage, Resources.CumManifiestoObtenerRemesas, requestDTO.InterfaceReference);
        data.UpdateRequestDetail(requestDetail);
        data.UpdateRequest(requestDTO);
        data.UpdateResponses(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.ErrorDescription, IsSucessful.IsNotSucessful, Resources.CumManifiestoRegistrarRespuesta);
        //gestion del error
        //requestDTO.ReportERROR
        return;
      }

      //Reportamos todas las cargas una a una
      if (dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
      {
        for (var i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        {
          var isRegister = dataSet.Tables[0].Rows[i][2].ToString();
          if (!String.IsNullOrEmpty(isRegister)) continue;

          int timeOut;
          int.TryParse(ConfigurationManager.AppSettings["WAITBETWEENREQUETS"], out timeOut);
          Thread.Sleep(timeOut);
          requestDetail.Reference = dataSet.Tables[0].Rows[i][0].ToString();
          if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.CumplidoRemesa))
          {
            interfaceSuccesful = false;
            //aqui para que se sobrepise esta variable en el LOOP
            data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription,
                                 IsSucessful.IsNotSucessful, Resources.CumRemesaRegistrarRespuesta);
            data.UpdateRequestDetail(requestDetail);
            //afectar el pedido ASTRANS con el error (SP)
          }
          else
          {
            data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, null,
                                 IsSucessful.Sucessful, Resources.CumRemesaRegistrarRespuesta);
            data.UpdateRequestDetail(requestDetail);
            //afectar el pedido ASTRANS con el idTransaccion, limpiamos Error Description (respons del SP)
          }
        }

        if (interfaceSuccesful)
        {
          requestDetail.Reference = requestDTO.InterfaceReference;
          requestDetail.ActionId = (int) RequestType.ActionId.Registrar;
          requestDetail.Processid = (int)RequestType.ProcessId.CumplirManifiestoDeCarga;

          if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.CumplidoManifiesto))
          {
            data.UpdateResponses(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.ErrorDescription,
                                 IsSucessful.IsNotSucessful, Resources.CumManifiestoRegistrarRespuesta);
            data.UpdateRequestDetail(requestDetail);
            //afectar el VIAJE ASTRANS con las malas noticias
          }
          else
          {
            data.UpdateResponses(requestDetail.TransactionId, requestDTO.InterfaceReference, null,
                                 IsSucessful.Sucessful, Resources.CumManifiestoRegistrarRespuesta);
            data.UpdateRequestDetail(requestDetail);
            requestDTO.Sucessful = true;
            requestDTO.TransactionId = requestDetail.TransactionId;
            //afectar el VIAJE ASTRANS con el idTransaccion, limpiamos Error Description (respons del SP)
          }

        }
        else
        {
          data.UpdateResponses(requestDetail.TransactionId, requestDTO.InterfaceReference, requestDetail.ErrorDescription,
                               IsSucessful.IsNotSucessful, Resources.CumManifiestoRegistrarRespuesta);
          //afectar el VIAJE ASTRANS con las malas noticias, error reportando pedido
        }

        data.UpdateRequest(requestDTO);
      }
    }
  }
  }
      