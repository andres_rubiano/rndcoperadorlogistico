﻿using Common;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.Main.Properties;

namespace Simplexity.AsTrans.RNDC.BusinessRules.Main
{
  public class RegisterLocations
  {
    public void ProcessRegisterLocations(RequestDTO requestDTO)
    {
      var data = new Data();
      var transactions = new Transactions();
      const int processId = (int)RequestType.ProcessId.CrearOActualizarDatosDeTercero;
      const int actionId = (int)RequestType.ActionId.Registrar;

      var requestDetail = new RequestDetailDTO
                            {
                              RequestId = requestDTO.RequestId,
                              ActionId = actionId,
                              Processid = processId,
                              Reference = requestDTO.InterfaceReference
                            };

      if (!transactions.TransactDetail(requestDTO, requestDetail, Resources.Sedes))
      {
        data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.IsNotSucessful, Resources.SedesRegistrarRespuesta);
        data.UpdateRequestDetail(requestDetail);
      }
      else
      {
        data.UpdateResponses(requestDetail.TransactionId, requestDetail.Reference, requestDetail.ErrorDescription, IsSucessful.Sucessful, Resources.SedesRegistrarRespuesta);
        data.UpdateRequestDetail(requestDetail);
        requestDTO.Sucessful = true;
        requestDTO.TransactionId = requestDetail.TransactionId;
      }
      data.UpdateRequest(requestDTO);
    }
  }
}
