﻿using System;

namespace Common
{
    public static class OperationConstants
    {
        public enum Status
        {
            NonProcess = 0,
            Processed = 1
        }

        public enum ResponseType
        {
            Error = 0,
            Successful = 1
        }
    }

    public static class TagsXml
    {
        public const string Tag = "<{0}>{1}</{2}>";
        //public const string TagQuery = "<{0}>'{1}'</{2}>";
        public const string TagQuery = "<{0}>{1}</{2}>";
        public const string TagStartId = "<ingresoid>";
        public const string TagEndId = "</ingresoid>";
        public const string TagStartError = "<ErrorMSG>";
        public const string TagEndError = "</ErrorMSG>";
        public const string TagStartSeguridadQr = "<seguridadqr>";
        public const string TagEndSeguridadQr = "</seguridadqr>";
        public const string TagStartObservaciones = "<observacionesqr>";
        public const string TagEndObservaciones = "</observacionesqr>";

        public const string DuplicateStart = "DUPLICADO:";
        public const string RemDuplicateEnd = "Error REM";
        public const string ManDuplicateEnd = "Error MAN";
        public const string CumRemDuplicateEnd = "Error CRE";
        public const string CumManDuplicateEnd = "Error CMA";
        public const string IsRemDuplicate = "REM030";
        public const string IsManDuplicate = "MAN030";
        public const string IsCumRemDuplicate = "CRE030";
        public const string IsCumManDuplicate = "CMA030";
        public const string DuplicateIdEnd = " ";
        public const string DuplicateIdStart = ":";
        public const string ProcessIdRemesa = " procesoid =\"44\"";
        public const string ProcessIdManifiesto = " procesoid =\"43\"";
        public const string Split = ",";
    }

    public static class Interfaces
    {
        public const string RegisterVehicle = "REG-VEH";
        public const string RegisterThirdParty = "REG-TER";
        public const string RegisterLoadingOrder = "REG-OCA";
        public const string RegisterrManifest = "REG-MAN";
        public const string FulfillManifest = "REG-CUM";
        public const string RegisterLocations = "REG-DIR";
        public const string CancelRem = "ANU-REM";
        public const string CancelManifest = "ANU-MAN";
        public const string CorrectShipment = "COR-REM";
        public const string RegistrarCumplidoInicialRemesa = "REG-CIR";
        public const string AnularCumplidoInicialRemesa = "ANU-CIR";
        public const string ConsultarFirmaElectronicaManifiesto = "CFE-MAN";
    }

    public static class Clientes
    {
        public const string Cordicargas = "Corona";
        public const string Frimac = "Frimac";
        public const string Holcim = "Holcim";
        public const string Logitrans = "Logitrans";
        public const string Solla = "Solla";
        public const string Suppla = "Suppla";
        public const string Tempo = "Tempo";
        public const string Trc = "Trc";
        public const string Tyf = "Tyf";
        public const string Vigia = "Vigia";
    }

    public static class Responses
    {
        public enum Transaction
        {
            Successful = 1,
            Warnning = -1,
            Error = 0
        }

        public enum Service
        {
            Successfull = 1,
            Error = 0
        }
    }

    public static class IsSucessful
    {
        public const string Sucessful = "T";
        public const string IsNotSucessful = "F";
    }

    public static class RequestType
    {
        public enum ProcessId
        {
            RegistrarInformacionDeCarga = 1,
            RegistrarInformacionDeViaje = 2,
            ExpedirRemesaTerrestreDeCarga = 3,
            ExpedirManifiestoDeCarga = 4,
            CumplirRemesaTerrestreDeCarga = 5,
            CumplirManifiestoDeCarga = 6,
            AnularInformacionDeCarga = 7,
            AnularInformacionDelViaje = 8,
            AnularRemesaTerrestreDeCarga = 9,
            CrearOActualizarDatosDeTercero = 11,
            CrearOActualizarDatosDeVehiculo = 12,
            DiccionarioDeDatos = 17,
            DiccionarioDeErrores = 27,
            AnularManifiestoDeCarga = 32,
            CorregirRemesa = 38,
            CumplidoInicialRemesa = 45,
            AnularCumplidoInicialRemesa = 54,
            ConsultarFirmaElectronica = 73
        }

        public enum ActionId
        {
            Registrar = 1,
            Consultar = 3
        }
    }

    public class StoredProcedure
    {
        public const string SpRndcManifiestoRegistrarRespuestaFirmaElectronica =
            "Sp_Rndc_ManifiestoRegistrarRespuestaFirmaElectronica";
    }

}