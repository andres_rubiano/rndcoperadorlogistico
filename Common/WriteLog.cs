﻿using System;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;

namespace Common
{
    public class LogRecord<T>
    {
        private T _object;

        public LogRecord(T t)
        {
            this._object = t;
        }

        public void WriteLog(T t, string fileName)
        {
            this._object = t;
            WriteLog(fileName);
        }

        public void WriteLog(string fileName)
        {
            bool traceability;
            bool.TryParse(ConfigurationManager.AppSettings["LogRequestResponseRndc"], out traceability);

            if (!traceability)
            {
                return;
            }

            string path = AppDomain.CurrentDomain.BaseDirectory + @"logs\";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            string pathFileName = string.Empty;

            pathFileName = string.Format(path + "{0}.txt", (fileName ?? "NoFileName") + "_" + DateTime.Now.ToString("yyyymmdd_HHmmss"));

            FileStream fs = new FileStream(pathFileName, FileMode.OpenOrCreate);

            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(JsonConvert.SerializeObject(_object, Formatting.Indented));
            }

            fs.Close();
        }
    }
}
