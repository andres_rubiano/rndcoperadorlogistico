﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Common
{
    public static class XmlTool
    {
        public static string GetValue(string xmlString, string pathPrincipal, string findTag)
        {
            var diccionario = new Dictionary<string, string>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            var demo = string.Format(@"/{0}", pathPrincipal);
            var nodes = xmlDoc.SelectNodes(demo);

            foreach (XmlNode data in nodes)
            {
                foreach (XmlNode tagXml in data)
                    diccionario.Add(tagXml.Name.ToLower(), tagXml.InnerText);

            }
            
            if (diccionario.Any())
                return diccionario[findTag.ToLower()];
            return string.Empty;
        }
    }
}
