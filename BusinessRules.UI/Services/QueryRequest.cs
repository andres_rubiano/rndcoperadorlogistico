﻿using System.Text;
using Common;
using System.Collections.Generic;
using Simplexity.AsTrans.RNDC.BusinessRules.UI.DTOs;
using Simplexity.AsTrans.RNDC.BusinessRules.UI.co.gov.mintransporte.rndc;

namespace Simplexity.AsTrans.RNDC.BusinessRules.UI.Services
{
    public class QueryRequest : Base
    {
        public string GetImformation(List<ClassQuery> listClassQuery, int processId)
        {
            var formattedXMLVariables = new StringBuilder();
            var formattedXMLDocumento = new StringBuilder();
            string TemplateXml = Resources.TemplateQueryXml;
            string formattedXML, response;

            foreach (var classQuery in listClassQuery)
            {
                if (classQuery.fielQuery)
                    formattedXMLVariables.Append(classQuery.field + TagsXml.Split);

                if (classQuery.fieldCondition != null)
                    formattedXMLDocumento.Append(string.Format(TagsXml.TagQuery, classQuery.field, classQuery.fieldCondition,
                                                               classQuery.field));
            }

            if (formattedXMLVariables.Length > 1)
                formattedXMLVariables.Replace(',', ' ', formattedXMLVariables.Length - 1, 1);

            formattedXML = string.Format(TemplateXml, User, Password, (int)RequestType.ActionId.Consultar,
                                         processId, formattedXMLVariables, formattedXMLDocumento);

            response = AtenderMensajeRndc(formattedXML);

            return response;

        }
    }
}
