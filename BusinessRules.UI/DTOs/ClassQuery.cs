﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.AsTrans.RNDC.BusinessRules.UI.DTOs
{
  public class ClassQuery
  {
    public string field { get; set; }
    public bool fielQuery { get; set; }
    public string fieldCondition { get; set; }
  }
}
